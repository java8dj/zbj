package com.ruoyi.project.student.coverAssessQuestionRef.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.coverAssessQuestionRef.domain.CoverAssessQuestionRef;
import com.ruoyi.project.student.coverAssessQuestionRef.service.ICoverAssessQuestionRefService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 被评测类型问题关联Controller
 * 
 * @author aa
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/student/coverAssessQuestionRef")
public class CoverAssessQuestionRefController extends BaseController
{
    private String prefix = "student/coverAssessQuestionRef";

    @Autowired
    private ICoverAssessQuestionRefService coverAssessQuestionRefService;

    @RequiresPermissions("student:coverAssessQuestionRef:view")
    @GetMapping()
    public String coverAssessQuestionRef()
    {
        return prefix + "/coverAssessQuestionRef";
    }

    /**
     * 查询被评测类型问题关联列表
     */
    @RequiresPermissions("student:coverAssessQuestionRef:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CoverAssessQuestionRef coverAssessQuestionRef)
    {
        startPage();
        List<CoverAssessQuestionRef> list = coverAssessQuestionRefService.selectCoverAssessQuestionRefList(coverAssessQuestionRef);
        return getDataTable(list);
    }

    /**
     * 导出被评测类型问题关联列表
     */
    @RequiresPermissions("student:coverAssessQuestionRef:export")
    @Log(title = "被评测类型问题关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CoverAssessQuestionRef coverAssessQuestionRef)
    {
        List<CoverAssessQuestionRef> list = coverAssessQuestionRefService.selectCoverAssessQuestionRefList(coverAssessQuestionRef);
        ExcelUtil<CoverAssessQuestionRef> util = new ExcelUtil<CoverAssessQuestionRef>(CoverAssessQuestionRef.class);
        return util.exportExcel(list, "coverAssessQuestionRef");
    }

    /**
     * 新增被评测类型问题关联
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存被评测类型问题关联
     */
    @RequiresPermissions("student:coverAssessQuestionRef:add")
    @Log(title = "被评测类型问题关联", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CoverAssessQuestionRef coverAssessQuestionRef)
    {
        return toAjax(coverAssessQuestionRefService.insertCoverAssessQuestionRef(coverAssessQuestionRef));
    }

    /**
     * 修改被评测类型问题关联
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CoverAssessQuestionRef coverAssessQuestionRef = coverAssessQuestionRefService.selectCoverAssessQuestionRefById(id);
        mmap.put("coverAssessQuestionRef", coverAssessQuestionRef);
        return prefix + "/edit";
    }

    /**
     * 修改保存被评测类型问题关联
     */
    @RequiresPermissions("student:coverAssessQuestionRef:edit")
    @Log(title = "被评测类型问题关联", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CoverAssessQuestionRef coverAssessQuestionRef)
    {
        return toAjax(coverAssessQuestionRefService.updateCoverAssessQuestionRef(coverAssessQuestionRef));
    }

    /**
     * 删除被评测类型问题关联
     */
    @RequiresPermissions("student:coverAssessQuestionRef:remove")
    @Log(title = "被评测类型问题关联", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(coverAssessQuestionRefService.deleteCoverAssessQuestionRefByIds(ids));
    }
}
