package com.ruoyi.project.student.coverAssessQuestionRef.mapper;

import java.util.List;
import com.ruoyi.project.student.coverAssessQuestionRef.domain.CoverAssessQuestionRef;

/**
 * 被评测类型问题关联Mapper接口
 * 
 * @author aa
 * @date 2021-01-29
 */
public interface CoverAssessQuestionRefMapper 
{
    /**
     * 查询被评测类型问题关联
     * 
     * @param id 被评测类型问题关联ID
     * @return 被评测类型问题关联
     */
    public CoverAssessQuestionRef selectCoverAssessQuestionRefById(Long id);

    /**
     * 查询被评测类型问题关联列表
     * 
     * @param coverAssessQuestionRef 被评测类型问题关联
     * @return 被评测类型问题关联集合
     */
    public List<CoverAssessQuestionRef> selectCoverAssessQuestionRefList(CoverAssessQuestionRef coverAssessQuestionRef);

    /**
     * 新增被评测类型问题关联
     * 
     * @param coverAssessQuestionRef 被评测类型问题关联
     * @return 结果
     */
    public int insertCoverAssessQuestionRef(CoverAssessQuestionRef coverAssessQuestionRef);

    /**
     * 修改被评测类型问题关联
     * 
     * @param coverAssessQuestionRef 被评测类型问题关联
     * @return 结果
     */
    public int updateCoverAssessQuestionRef(CoverAssessQuestionRef coverAssessQuestionRef);

    /**
     * 删除被评测类型问题关联
     * 
     * @param id 被评测类型问题关联ID
     * @return 结果
     */
    public int deleteCoverAssessQuestionRefById(Long id);

    /**
     * 批量删除被评测类型问题关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCoverAssessQuestionRefByIds(String[] ids);

    List<CoverAssessQuestionRef> selectCoverAssessQuestionRefByCoverAssessId(Long id);

    int deleteCoverAssessQuestionRefByCoverAssessId(Long id);
}
