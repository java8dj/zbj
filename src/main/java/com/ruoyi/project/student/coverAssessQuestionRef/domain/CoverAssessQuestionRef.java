package com.ruoyi.project.student.coverAssessQuestionRef.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 被评测类型问题关联对象 lg_cover_assess_question_ref
 * 
 * @author aa
 * @date 2021-01-29
 */
public class CoverAssessQuestionRef extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 被评测类型 */
    @Excel(name = "被评测类型")
    private Long coverAssessId;

    /** 问题 */
    @Excel(name = "问题")
    private Long questionId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCoverAssessId(Long coverAssessId)
    {
        this.coverAssessId = coverAssessId;
    }

    public Long getCoverAssessId()
    {
        return coverAssessId;
    }
    public void setQuestionId(Long questionId)
    {
        this.questionId = questionId;
    }

    public Long getQuestionId()
    {
        return questionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("coverAssessId", getCoverAssessId())
            .append("questionId", getQuestionId())
            .toString();
    }
}
