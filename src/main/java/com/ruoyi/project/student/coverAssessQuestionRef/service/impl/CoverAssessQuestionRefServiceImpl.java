package com.ruoyi.project.student.coverAssessQuestionRef.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.coverAssessQuestionRef.mapper.CoverAssessQuestionRefMapper;
import com.ruoyi.project.student.coverAssessQuestionRef.domain.CoverAssessQuestionRef;
import com.ruoyi.project.student.coverAssessQuestionRef.service.ICoverAssessQuestionRefService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 被评测类型问题关联Service业务层处理
 * 
 * @author aa
 * @date 2021-01-29
 */
@Service
public class CoverAssessQuestionRefServiceImpl implements ICoverAssessQuestionRefService 
{
    @Autowired
    private CoverAssessQuestionRefMapper coverAssessQuestionRefMapper;

    /**
     * 查询被评测类型问题关联
     * 
     * @param id 被评测类型问题关联ID
     * @return 被评测类型问题关联
     */
    @Override
    public CoverAssessQuestionRef selectCoverAssessQuestionRefById(Long id)
    {
        return coverAssessQuestionRefMapper.selectCoverAssessQuestionRefById(id);
    }

    /**
     * 查询被评测类型问题关联列表
     * 
     * @param coverAssessQuestionRef 被评测类型问题关联
     * @return 被评测类型问题关联
     */
    @Override
    public List<CoverAssessQuestionRef> selectCoverAssessQuestionRefList(CoverAssessQuestionRef coverAssessQuestionRef)
    {
        return coverAssessQuestionRefMapper.selectCoverAssessQuestionRefList(coverAssessQuestionRef);
    }

    /**
     * 新增被评测类型问题关联
     * 
     * @param coverAssessQuestionRef 被评测类型问题关联
     * @return 结果
     */
    @Override
    public int insertCoverAssessQuestionRef(CoverAssessQuestionRef coverAssessQuestionRef)
    {
        return coverAssessQuestionRefMapper.insertCoverAssessQuestionRef(coverAssessQuestionRef);
    }

    /**
     * 修改被评测类型问题关联
     * 
     * @param coverAssessQuestionRef 被评测类型问题关联
     * @return 结果
     */
    @Override
    public int updateCoverAssessQuestionRef(CoverAssessQuestionRef coverAssessQuestionRef)
    {
        return coverAssessQuestionRefMapper.updateCoverAssessQuestionRef(coverAssessQuestionRef);
    }

    /**
     * 删除被评测类型问题关联对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCoverAssessQuestionRefByIds(String ids)
    {
        return coverAssessQuestionRefMapper.deleteCoverAssessQuestionRefByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除被评测类型问题关联信息
     * 
     * @param id 被评测类型问题关联ID
     * @return 结果
     */
    @Override
    public int deleteCoverAssessQuestionRefById(Long id)
    {
        return coverAssessQuestionRefMapper.deleteCoverAssessQuestionRefById(id);
    }
}
