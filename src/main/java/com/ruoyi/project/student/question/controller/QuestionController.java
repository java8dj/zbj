package com.ruoyi.project.student.question.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.question.domain.Question;
import com.ruoyi.project.student.question.service.IQuestionService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 问题Controller
 * 
 * @author aa
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/student/question")
public class QuestionController extends BaseController
{
    private String prefix = "student/question";

    @Autowired
    private IQuestionService questionService;

    @RequiresPermissions("student:question:view")
    @GetMapping()
    public String question()
    {
        return prefix + "/question";
    }

    /**
     * 查询问题列表
     */
    @RequiresPermissions("student:question:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Question question)
    {
        startPage();
        List<Question> list = questionService.selectQuestionList(question);
        return getDataTable(list);
    }

    /**
     * 导出问题列表
     */
    @RequiresPermissions("student:question:export")
    @Log(title = "问题", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Question question)
    {
        List<Question> list = questionService.selectQuestionList(question);
        ExcelUtil<Question> util = new ExcelUtil<Question>(Question.class);
        return util.exportExcel(list, "question");
    }

    /**
     * 新增问题
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存问题
     */
    @RequiresPermissions("student:question:add")
    @Log(title = "问题", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Question question)
    {
        question.itemsToContext();
        return toAjax(questionService.insertQuestion(question));
    }

    /**
     * 修改问题
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Question question = questionService.selectQuestionById(id);
        question.contextToItems();
        mmap.put("question", question);
        return prefix + "/edit";
    }

    /**
     * 修改保存问题
     */
    @RequiresPermissions("student:question:edit")
    @Log(title = "问题", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Question question)
    {
        question.itemsToContext();
        return toAjax(questionService.updateQuestion(question));
    }

    /**
     * 删除问题
     */
    @RequiresPermissions("student:question:remove")
    @Log(title = "问题", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(questionService.deleteQuestionByIds(ids));
    }
}
