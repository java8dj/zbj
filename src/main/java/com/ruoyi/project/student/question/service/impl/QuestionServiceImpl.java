package com.ruoyi.project.student.question.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.question.mapper.QuestionMapper;
import com.ruoyi.project.student.question.domain.Question;
import com.ruoyi.project.student.question.service.IQuestionService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 问题Service业务层处理
 * 
 * @author aa
 * @date 2021-01-29
 */
@Service
public class QuestionServiceImpl implements IQuestionService 
{
    @Autowired
    private QuestionMapper questionMapper;

    /**
     * 查询问题
     * 
     * @param id 问题ID
     * @return 问题
     */
    @Override
    public Question selectQuestionById(Long id)
    {
        return questionMapper.selectQuestionById(id);
    }

    /**
     * 查询问题列表
     * 
     * @param question 问题
     * @return 问题
     */
    @Override
    public List<Question> selectQuestionList(Question question)
    {
        return questionMapper.selectQuestionList(question);
    }

    /**
     * 新增问题
     * 
     * @param question 问题
     * @return 结果
     */
    @Override
    public int insertQuestion(Question question)
    {
        question.setCreateTime(DateUtils.getNowDate());
        return questionMapper.insertQuestion(question);
    }

    /**
     * 修改问题
     * 
     * @param question 问题
     * @return 结果
     */
    @Override
    public int updateQuestion(Question question)
    {
        question.setUpdateTime(DateUtils.getNowDate());
        return questionMapper.updateQuestion(question);
    }

    /**
     * 删除问题对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteQuestionByIds(String ids)
    {
        return questionMapper.deleteQuestionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除问题信息
     * 
     * @param id 问题ID
     * @return 结果
     */
    @Override
    public int deleteQuestionById(Long id)
    {
        return questionMapper.deleteQuestionById(id);
    }

    @Override
    public List<Question> selectQuestionAll() {
        return questionMapper.selectQuestionAll();
    }
}
