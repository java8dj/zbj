package com.ruoyi.project.student.question.domain;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.StringUtils;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * 问题对象 lg_question
 * 
 * @author aa
 * @date 2021-01-29
 */
@Data
public class Question extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 问题标题 */
    @Excel(name = "问题标题")
    private String title;

    /** 选择数量 */
    @Excel(name = "选择数量")
    private Long choiceTimes;

    /** 内容(json格式) */
    @Excel(name = "内容(json格式)")
    private String context;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    private Integer itemA;
    private Integer itemB;
    private Integer itemC;
    private Integer itemD;

    public void itemsToContext(){
        Map<String,Integer> itemMap = new HashMap<>();
        if(null!=getItemA()){
            itemMap.put("A", getItemA());
        }
        if(null!=getItemA()){
            itemMap.put("B", getItemB());
        }
        if(null!=getItemA()){
            itemMap.put("C", getItemC());
        }
        if(null!=getItemA()){
            itemMap.put("D", getItemD());
        }
        setContext(JSONObject.toJSONString(itemMap));
    }

    public void contextToItems(){
        if(StringUtils.isNotBlank(getContext())){
            JSONObject jo = JSONObject.parseObject(getContext());
            if(jo.containsKey("A")){
                setItemA(jo.getInteger("A"));
            }
            if(jo.containsKey("B")){
                setItemB(jo.getInteger("B"));
            }
            if(jo.containsKey("C")){
                setItemC(jo.getInteger("C"));
            }
            if(jo.containsKey("D")){
                setItemD(jo.getInteger("D"));
            }
        }
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setChoiceTimes(Long choiceTimes)
    {
        this.choiceTimes = choiceTimes;
    }

    public Long getChoiceTimes()
    {
        return choiceTimes;
    }
    public void setContext(String context)
    {
        this.context = context;
    }

    public String getContext()
    {
        return context;
    }
    public void setSort(Long sort)
    {
        this.sort = sort;
    }

    public Long getSort()
    {
        return sort;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("choiceTimes", getChoiceTimes())
            .append("context", getContext())
            .append("sort", getSort())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
