package com.ruoyi.project.student.question.service;

import java.util.List;
import com.ruoyi.project.student.question.domain.Question;

/**
 * 问题Service接口
 * 
 * @author aa
 * @date 2021-01-29
 */
public interface IQuestionService 
{
    /**
     * 查询问题
     * 
     * @param id 问题ID
     * @return 问题
     */
    public Question selectQuestionById(Long id);

    /**
     * 查询问题列表
     * 
     * @param question 问题
     * @return 问题集合
     */
    public List<Question> selectQuestionList(Question question);

    /**
     * 新增问题
     * 
     * @param question 问题
     * @return 结果
     */
    public int insertQuestion(Question question);

    /**
     * 修改问题
     * 
     * @param question 问题
     * @return 结果
     */
    public int updateQuestion(Question question);

    /**
     * 批量删除问题
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteQuestionByIds(String ids);

    /**
     * 删除问题信息
     * 
     * @param id 问题ID
     * @return 结果
     */
    public int deleteQuestionById(Long id);

    public List<Question> selectQuestionAll();
}
