package com.ruoyi.project.student.activity.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.student.activity_item.domain.ActivityItem;
import com.ruoyi.project.student.activity_item.mapper.ActivityItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.activity.mapper.ActivityMapper;
import com.ruoyi.project.student.activity.domain.Activity;
import com.ruoyi.project.student.activity.service.IActivityService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 评测活动Service业务层处理
 * 
 * @author aa
 * @date 2021-01-29
 */
@Service
public class ActivityServiceImpl implements IActivityService 
{
    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private ActivityItemMapper activityItemMapper;

    /**
     * 查询评测活动
     * 
     * @param id 评测活动ID
     * @return 评测活动
     */
    @Override
    public Activity selectActivityById(Long id)
    {
        return activityMapper.selectActivityById(id);
    }

    /**
     * 查询评测活动列表
     * 
     * @param activity 评测活动
     * @return 评测活动
     */
    @Override
    public List<Activity> selectActivityList(Activity activity)
    {
        return activityMapper.selectActivityList(activity);
    }

    /**
     * 新增评测活动
     * 
     * @param activity 评测活动
     * @return 结果
     */
    @Override
    public int insertActivity(Activity activity)
    {
        activity.setCreateTime(DateUtils.getNowDate());
        int i = activityMapper.insertActivity(activity);
        // 插入item
        activity.getItems().forEach(e->{
            ActivityItem item = new ActivityItem();
            item.setActivityId(activity.getId());
            item.setCoverAssessId(e);
            activityItemMapper.insertActivityItem(item);
        });
        return i;
    }

    /**
     * 修改评测活动
     * 
     * @param activity 评测活动
     * @return 结果
     */
    @Override
    public int updateActivity(Activity activity)
    {
        activity.setUpdateTime(DateUtils.getNowDate());
        // 删除item
        activityItemMapper.deleteActivityItemByActivityId(activity.getId());

        // 插入item
        activity.getItems().forEach(e->{
            ActivityItem item = new ActivityItem();
            item.setActivityId(activity.getId());
            item.setCoverAssessId(e);
            activityItemMapper.insertActivityItem(item);
        });
        return activityMapper.updateActivity(activity);
    }

    /**
     * 删除评测活动对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActivityByIds(String ids)
    {
        return activityMapper.deleteActivityByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除评测活动信息
     * 
     * @param id 评测活动ID
     * @return 结果
     */
    @Override
    public int deleteActivityById(Long id)
    {
        return activityMapper.deleteActivityById(id);
    }
}
