package com.ruoyi.project.student.activity.mapper;

import java.util.List;
import com.ruoyi.project.student.activity.domain.Activity;

/**
 * 评测活动Mapper接口
 * 
 * @author aa
 * @date 2021-01-29
 */
public interface ActivityMapper 
{
    /**
     * 查询评测活动
     * 
     * @param id 评测活动ID
     * @return 评测活动
     */
    public Activity selectActivityById(Long id);

    /**
     * 查询评测活动列表
     * 
     * @param activity 评测活动
     * @return 评测活动集合
     */
    public List<Activity> selectActivityList(Activity activity);

    /**
     * 新增评测活动
     * 
     * @param activity 评测活动
     * @return 结果
     */
    public int insertActivity(Activity activity);

    /**
     * 修改评测活动
     * 
     * @param activity 评测活动
     * @return 结果
     */
    public int updateActivity(Activity activity);

    /**
     * 删除评测活动
     * 
     * @param id 评测活动ID
     * @return 结果
     */
    public int deleteActivityById(Long id);

    /**
     * 批量删除评测活动
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActivityByIds(String[] ids);
}
