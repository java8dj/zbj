package com.ruoyi.project.student.activity.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.project.student.activity_item.domain.ActivityItem;
import com.ruoyi.project.student.activity_item.service.IActivityItemService;
import com.ruoyi.project.student.assess.service.ICoverAssessService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.activity.domain.Activity;
import com.ruoyi.project.student.activity.service.IActivityService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 评测活动Controller
 * 
 * @author aa
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/student/activity")
public class ActivityController extends BaseController
{
    private String prefix = "student/activity";

    @Autowired
    private IActivityService activityService;
    @Autowired
    private ICoverAssessService coverAssessService;
    @Autowired
    private IActivityItemService activityItemService;

    @RequiresPermissions("student:activity:view")
    @GetMapping()
    public String activity()
    {
        return prefix + "/activity";
    }

    /**
     * 查询评测活动列表
     */
    @RequiresPermissions("student:activity:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Activity activity)
    {
        startPage();
        List<Activity> list = activityService.selectActivityList(activity);
        return getDataTable(list);
    }

    /**
     * 导出评测活动列表
     */
    @RequiresPermissions("student:activity:export")
    @Log(title = "评测活动", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Activity activity)
    {
        List<Activity> list = activityService.selectActivityList(activity);
        ExcelUtil<Activity> util = new ExcelUtil<Activity>(Activity.class);
        return util.exportExcel(list, "activity");
    }

    /**
     * 新增评测活动
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("coverAssessList", coverAssessService.selectCoverAssessAll());
        return prefix + "/add";
    }

    /**
     * 新增保存评测活动
     */
    @RequiresPermissions("student:activity:add")
    @Log(title = "评测活动", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Activity activity)
    {
        return toAjax(activityService.insertActivity(activity));
    }

    /**
     * 修改评测活动
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("coverAssessList", coverAssessService.selectCoverAssessAll());

        Activity activity = activityService.selectActivityById(id);

        ActivityItem q = new ActivityItem();
        q.setActivityId(id);
        List<ActivityItem> activityItems = activityItemService.selectActivityItemList(q);
        if(null!=activityItems && activityItems.size()>0){
            activity.setItems(
                    activityItems.stream().map(ActivityItem::getCoverAssessId).collect(Collectors.toList())
            );
        }

        mmap.put("activity", activity);
        return prefix + "/edit";
    }

    /**
     * 修改保存评测活动
     */
    @RequiresPermissions("student:activity:edit")
    @Log(title = "评测活动", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Activity activity)
    {
        return toAjax(activityService.updateActivity(activity));
    }

    /**
     * 删除评测活动
     */
    @RequiresPermissions("student:activity:remove")
    @Log(title = "评测活动", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(activityService.deleteActivityByIds(ids));
    }
}
