package com.ruoyi.project.student.answer_item.service;

import java.util.List;
import com.ruoyi.project.student.answer_item.domain.AssessAnswerItem;

/**
 * 答案详细Service接口
 * 
 * @author aa
 * @date 2021-01-29
 */
public interface IAssessAnswerItemService 
{
    /**
     * 查询答案详细
     * 
     * @param id 答案详细ID
     * @return 答案详细
     */
    public AssessAnswerItem selectAssessAnswerItemById(Long id);

    /**
     * 查询答案详细列表
     * 
     * @param assessAnswerItem 答案详细
     * @return 答案详细集合
     */
    public List<AssessAnswerItem> selectAssessAnswerItemList(AssessAnswerItem assessAnswerItem);

    /**
     * 新增答案详细
     * 
     * @param assessAnswerItem 答案详细
     * @return 结果
     */
    public int insertAssessAnswerItem(AssessAnswerItem assessAnswerItem);

    /**
     * 修改答案详细
     * 
     * @param assessAnswerItem 答案详细
     * @return 结果
     */
    public int updateAssessAnswerItem(AssessAnswerItem assessAnswerItem);

    /**
     * 批量删除答案详细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAssessAnswerItemByIds(String ids);

    /**
     * 删除答案详细信息
     * 
     * @param id 答案详细ID
     * @return 结果
     */
    public int deleteAssessAnswerItemById(Long id);
}
