package com.ruoyi.project.student.answer_item.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.answer_item.domain.AssessAnswerItem;
import com.ruoyi.project.student.answer_item.service.IAssessAnswerItemService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 答案详细Controller
 * 
 * @author aa
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/student/answer_item")
public class AssessAnswerItemController extends BaseController
{
    private String prefix = "student/answer_item";

    @Autowired
    private IAssessAnswerItemService assessAnswerItemService;

    @RequiresPermissions("student:answer_item:view")
    @GetMapping()
    public String answer_item()
    {
        return prefix + "/answer_item";
    }

    /**
     * 查询答案详细列表
     */
    @RequiresPermissions("student:answer_item:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(AssessAnswerItem assessAnswerItem)
    {
        startPage();
        List<AssessAnswerItem> list = assessAnswerItemService.selectAssessAnswerItemList(assessAnswerItem);
        return getDataTable(list);
    }

    /**
     * 导出答案详细列表
     */
    @RequiresPermissions("student:answer_item:export")
    @Log(title = "答案详细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(AssessAnswerItem assessAnswerItem)
    {
        List<AssessAnswerItem> list = assessAnswerItemService.selectAssessAnswerItemList(assessAnswerItem);
        ExcelUtil<AssessAnswerItem> util = new ExcelUtil<AssessAnswerItem>(AssessAnswerItem.class);
        return util.exportExcel(list, "answer_item");
    }

    /**
     * 新增答案详细
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存答案详细
     */
    @RequiresPermissions("student:answer_item:add")
    @Log(title = "答案详细", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(AssessAnswerItem assessAnswerItem)
    {
        return toAjax(assessAnswerItemService.insertAssessAnswerItem(assessAnswerItem));
    }

    /**
     * 修改答案详细
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        AssessAnswerItem assessAnswerItem = assessAnswerItemService.selectAssessAnswerItemById(id);
        mmap.put("assessAnswerItem", assessAnswerItem);
        return prefix + "/edit";
    }

    /**
     * 修改保存答案详细
     */
    @RequiresPermissions("student:answer_item:edit")
    @Log(title = "答案详细", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(AssessAnswerItem assessAnswerItem)
    {
        return toAjax(assessAnswerItemService.updateAssessAnswerItem(assessAnswerItem));
    }

    /**
     * 删除答案详细
     */
    @RequiresPermissions("student:answer_item:remove")
    @Log(title = "答案详细", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(assessAnswerItemService.deleteAssessAnswerItemByIds(ids));
    }
}
