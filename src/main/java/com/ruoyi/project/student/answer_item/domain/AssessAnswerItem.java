package com.ruoyi.project.student.answer_item.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 答案详细对象 lg_assess_answer_item
 * 
 * @author aa
 * @date 2021-01-29
 */
public class AssessAnswerItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 主表 */
    @Excel(name = "主表")
    private Long answerId;

    /** 问题 */
    @Excel(name = "问题")
    private String questionTitle;

    /** 问题选项 */
    @Excel(name = "问题选项")
    private String context;

    /** 回答选线 */
    @Excel(name = "回答选线")
    private String answerContext;

    /** 得分 */
    @Excel(name = "得分")
    private Long score;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setAnswerId(Long answerId)
    {
        this.answerId = answerId;
    }

    public Long getAnswerId()
    {
        return answerId;
    }
    public void setQuestionTitle(String questionTitle)
    {
        this.questionTitle = questionTitle;
    }

    public String getQuestionTitle()
    {
        return questionTitle;
    }
    public void setContext(String context)
    {
        this.context = context;
    }

    public String getContext()
    {
        return context;
    }
    public void setAnswerContext(String answerContext)
    {
        this.answerContext = answerContext;
    }

    public String getAnswerContext()
    {
        return answerContext;
    }
    public void setScore(Long score)
    {
        this.score = score;
    }

    public Long getScore()
    {
        return score;
    }
    public void setSort(Long sort)
    {
        this.sort = sort;
    }

    public Long getSort()
    {
        return sort;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("answerId", getAnswerId())
            .append("questionTitle", getQuestionTitle())
            .append("context", getContext())
            .append("answerContext", getAnswerContext())
            .append("score", getScore())
            .append("sort", getSort())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
