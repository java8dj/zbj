package com.ruoyi.project.student.answer_item.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.answer_item.mapper.AssessAnswerItemMapper;
import com.ruoyi.project.student.answer_item.domain.AssessAnswerItem;
import com.ruoyi.project.student.answer_item.service.IAssessAnswerItemService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 答案详细Service业务层处理
 * 
 * @author aa
 * @date 2021-01-29
 */
@Service
public class AssessAnswerItemServiceImpl implements IAssessAnswerItemService 
{
    @Autowired
    private AssessAnswerItemMapper assessAnswerItemMapper;

    /**
     * 查询答案详细
     * 
     * @param id 答案详细ID
     * @return 答案详细
     */
    @Override
    public AssessAnswerItem selectAssessAnswerItemById(Long id)
    {
        return assessAnswerItemMapper.selectAssessAnswerItemById(id);
    }

    /**
     * 查询答案详细列表
     * 
     * @param assessAnswerItem 答案详细
     * @return 答案详细
     */
    @Override
    public List<AssessAnswerItem> selectAssessAnswerItemList(AssessAnswerItem assessAnswerItem)
    {
        return assessAnswerItemMapper.selectAssessAnswerItemList(assessAnswerItem);
    }

    /**
     * 新增答案详细
     * 
     * @param assessAnswerItem 答案详细
     * @return 结果
     */
    @Override
    public int insertAssessAnswerItem(AssessAnswerItem assessAnswerItem)
    {
        assessAnswerItem.setCreateTime(DateUtils.getNowDate());
        return assessAnswerItemMapper.insertAssessAnswerItem(assessAnswerItem);
    }

    /**
     * 修改答案详细
     * 
     * @param assessAnswerItem 答案详细
     * @return 结果
     */
    @Override
    public int updateAssessAnswerItem(AssessAnswerItem assessAnswerItem)
    {
        assessAnswerItem.setUpdateTime(DateUtils.getNowDate());
        return assessAnswerItemMapper.updateAssessAnswerItem(assessAnswerItem);
    }

    /**
     * 删除答案详细对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAssessAnswerItemByIds(String ids)
    {
        return assessAnswerItemMapper.deleteAssessAnswerItemByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除答案详细信息
     * 
     * @param id 答案详细ID
     * @return 结果
     */
    @Override
    public int deleteAssessAnswerItemById(Long id)
    {
        return assessAnswerItemMapper.deleteAssessAnswerItemById(id);
    }
}
