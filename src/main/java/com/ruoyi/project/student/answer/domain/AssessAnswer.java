package com.ruoyi.project.student.answer.domain;

import com.ruoyi.project.student.answer_item.domain.AssessAnswerItem;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.List;

/**
 * 答案对象 lg_assess_answer
 * 
 * @author aa
 * @date 2021-01-29
 */
@Data
public class AssessAnswer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 活动 */
    @Excel(name = "活动")
    private Long activityId;
    private String activityName;

    /** 活动项 */
    @Excel(name = "活动项")
    private Long activityItemId;

    /** 发表评测用户 */
    @Excel(name = "发表评测用户")
    private Long userId;
    private String userName;

    /** 评测用户部门 */
    @Excel(name = "评测用户部门")
    private Long userDeptId;
    private String userDeptName;

    /** 评测用户岗位 */
    @Excel(name = "评测用户岗位")
    private Long userPostId;
    private String userPostName;

    /** 被评测用户 */
    @Excel(name = "被评测用户")
    private Long coverUserId;
    private String coverUserName;

    /** 被评测用户部门 */
    @Excel(name = "被评测用户部门")
    private Long coverUserDeptId;
    private String coverUserDeptName;

    /** 被评测用户岗位 */
    @Excel(name = "被评测用户岗位")
    private Long coverUserPostId;
    private String coverUserPostName;


    /** 得分 */
    @Excel(name = "得分")
    private Long score;

    /** 权重得分 */
    @Excel(name = "权重得分")
    private Long weightScore;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    private List<AssessAnswerItem> assessAnswerItemList;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setActivityId(Long activityId)
    {
        this.activityId = activityId;
    }

    public Long getActivityId()
    {
        return activityId;
    }
    public void setActivityItemId(Long activityItemId)
    {
        this.activityItemId = activityItemId;
    }

    public Long getActivityItemId()
    {
        return activityItemId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserDeptId(Long userDeptId)
    {
        this.userDeptId = userDeptId;
    }

    public Long getUserDeptId()
    {
        return userDeptId;
    }
    public void setUserPostId(Long userPostId)
    {
        this.userPostId = userPostId;
    }

    public Long getUserPostId()
    {
        return userPostId;
    }
    public void setCoverUserId(Long coverUserId)
    {
        this.coverUserId = coverUserId;
    }

    public Long getCoverUserId()
    {
        return coverUserId;
    }
    public void setCoverUserDeptId(Long coverUserDeptId)
    {
        this.coverUserDeptId = coverUserDeptId;
    }

    public Long getCoverUserDeptId()
    {
        return coverUserDeptId;
    }
    public void setCoverUserPostId(Long coverUserPostId)
    {
        this.coverUserPostId = coverUserPostId;
    }

    public Long getCoverUserPostId()
    {
        return coverUserPostId;
    }
    public void setScore(Long score)
    {
        this.score = score;
    }

    public Long getScore()
    {
        return score;
    }
    public void setWeightScore(Long weightScore)
    {
        this.weightScore = weightScore;
    }

    public Long getWeightScore()
    {
        return weightScore;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("activityId", getActivityId())
            .append("activityItemId", getActivityItemId())
            .append("userId", getUserId())
            .append("userDeptId", getUserDeptId())
            .append("userPostId", getUserPostId())
            .append("coverUserId", getCoverUserId())
            .append("coverUserDeptId", getCoverUserDeptId())
            .append("coverUserPostId", getCoverUserPostId())
            .append("score", getScore())
            .append("weightScore", getWeightScore())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
