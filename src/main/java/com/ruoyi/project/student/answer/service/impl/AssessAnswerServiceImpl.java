package com.ruoyi.project.student.answer.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.answer.mapper.AssessAnswerMapper;
import com.ruoyi.project.student.answer.domain.AssessAnswer;
import com.ruoyi.project.student.answer.service.IAssessAnswerService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 答案Service业务层处理
 * 
 * @author aa
 * @date 2021-01-29
 */
@Service
public class AssessAnswerServiceImpl implements IAssessAnswerService 
{
    @Autowired
    private AssessAnswerMapper assessAnswerMapper;

    /**
     * 查询答案
     * 
     * @param id 答案ID
     * @return 答案
     */
    @Override
    public AssessAnswer selectAssessAnswerById(Long id)
    {
        return assessAnswerMapper.selectAssessAnswerById(id);
    }

    /**
     * 查询答案列表
     * 
     * @param assessAnswer 答案
     * @return 答案
     */
    @Override
    public List<AssessAnswer> selectAssessAnswerList(AssessAnswer assessAnswer)
    {
        return assessAnswerMapper.selectAssessAnswerList(assessAnswer);
    }

    /**
     * 新增答案
     * 
     * @param assessAnswer 答案
     * @return 结果
     */
    @Override
    public int insertAssessAnswer(AssessAnswer assessAnswer)
    {
        assessAnswer.setCreateTime(DateUtils.getNowDate());
        return assessAnswerMapper.insertAssessAnswer(assessAnswer);
    }

    /**
     * 修改答案
     * 
     * @param assessAnswer 答案
     * @return 结果
     */
    @Override
    public int updateAssessAnswer(AssessAnswer assessAnswer)
    {
        assessAnswer.setUpdateTime(DateUtils.getNowDate());
        return assessAnswerMapper.updateAssessAnswer(assessAnswer);
    }

    /**
     * 删除答案对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAssessAnswerByIds(String ids)
    {
        return assessAnswerMapper.deleteAssessAnswerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除答案信息
     * 
     * @param id 答案ID
     * @return 结果
     */
    @Override
    public int deleteAssessAnswerById(Long id)
    {
        return assessAnswerMapper.deleteAssessAnswerById(id);
    }
}
