package com.ruoyi.project.student.answer.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.project.student.activity_item.domain.ActivityItem;
import com.ruoyi.project.student.activity_item.service.IActivityItemService;
import com.ruoyi.project.student.answer_item.domain.AssessAnswerItem;
import com.ruoyi.project.student.answer_item.service.IAssessAnswerItemService;
import com.ruoyi.project.system.dept.service.IDeptService;
import com.ruoyi.project.system.post.domain.Post;
import com.ruoyi.project.system.post.service.IPostService;
import com.ruoyi.project.system.user.domain.User;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.answer.domain.AssessAnswer;
import com.ruoyi.project.student.answer.service.IAssessAnswerService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 答案Controller
 * 
 * @author aa
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/student/answer")
public class AssessAnswerController extends BaseController
{
    private String prefix = "student/answer";

    @Autowired
    private IAssessAnswerService assessAnswerService;
    @Autowired
    private IAssessAnswerItemService assessAnswerItemService;
    @Autowired
    private IActivityItemService activityItemService;
    @Autowired
    private IDeptService deptService;
    @Autowired
    private IPostService postService;

    @RequiresPermissions("student:answer:view")
    @GetMapping()
    public String answer()
    {
        return prefix + "/answer";
    }

    /**
     * 查询答案列表
     */
    @RequiresPermissions("student:answer:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(AssessAnswer assessAnswer)
    {
        startPage();
        List<AssessAnswer> list = assessAnswerService.selectAssessAnswerList(assessAnswer);
        return getDataTable(list);
    }

    /**
     * 导出答案列表
     */
    @RequiresPermissions("student:answer:export")
    @Log(title = "答案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(AssessAnswer assessAnswer)
    {
        List<AssessAnswer> list = assessAnswerService.selectAssessAnswerList(assessAnswer);
        ExcelUtil<AssessAnswer> util = new ExcelUtil<AssessAnswer>(AssessAnswer.class);
        return util.exportExcel(list, "answer");
    }

    /**
     * 新增答案
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    @GetMapping("/info/{id}")
    public String info(@PathVariable("id") Long id, ModelMap mmap)
    {
        AssessAnswerItem q = new AssessAnswerItem();
        q.setAnswerId(id);
        mmap.put("assessAnswerItemList", assessAnswerItemService.selectAssessAnswerItemList(q));
        return prefix + "/info";
    }

    /**
     * 新增保存答案
     */
    @RequiresPermissions("student:answer:add")
    @Log(title = "答案", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(AssessAnswer assessAnswer)
    {
        User user = ShiroUtils.getSysUser();


        // 是否测评过
        AssessAnswer q = new AssessAnswer();
        q.setActivityItemId(assessAnswer.getActivityItemId());
        q.setUserId(user.getUserId());
        q.setCoverUserId(assessAnswer.getCoverUserId());
        List<AssessAnswer> has = assessAnswerService.selectAssessAnswerList(q);
        if(null!=has && has.size()>0){
            return AjaxResult.error("评测过 , 不能重复评测");
        }
//        if(has!=null){
//            has.forEach(e->{
//                assessAnswerService.deleteAssessAnswerById(e.getId());
//                AssessAnswerItem itm = new AssessAnswerItem();
//                itm.setAnswerId(e.getId());
//                assessAnswerItemService.selectAssessAnswerItemList(itm).forEach(e2->{
//                    assessAnswerItemService.deleteAssessAnswerItemById(e2.getId());
//                });
//            });
//        }


        assessAnswer.setUserId(user.getUserId());

        assessAnswer.setActivityId(
                activityItemService.selectActivityItemById(assessAnswer.getActivityItemId())
                .getActivityId()
        );

        assessAnswer.setScore(0l);
        assessAnswer.getAssessAnswerItemList().forEach(e->{
            if(null!=e.getScore()){
                assessAnswer.setScore(assessAnswer.getScore() + e.getScore());
            }
        });
//
//        assessAnswer.setUserDeptId(user.getDeptId());
//        List<Post> p1List = postService.selectPostsByUserId(user.getUserId()).stream().filter(Post::isFlag).collect(Collectors.toList());
//        if(null!=p1List && p1List.size()>0){
//            assessAnswer.set
//        }

        int result = assessAnswerService.insertAssessAnswer(assessAnswer);

        // 插入答题项
        assessAnswer.getAssessAnswerItemList().forEach(e->{
            e.setAnswerId(assessAnswer.getId());
            assessAnswerItemService.insertAssessAnswerItem(e);
        });

        return toAjax(result);
    }

    /**
     * 修改答案
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        AssessAnswer assessAnswer = assessAnswerService.selectAssessAnswerById(id);
        mmap.put("assessAnswer", assessAnswer);
        return prefix + "/edit";
    }

    /**
     * 修改保存答案
     */
    @RequiresPermissions("student:answer:edit")
    @Log(title = "答案", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(AssessAnswer assessAnswer)
    {
        return toAjax(assessAnswerService.updateAssessAnswer(assessAnswer));
    }

    /**
     * 删除答案
     */
    @RequiresPermissions("student:answer:remove")
    @Log(title = "答案", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(assessAnswerService.deleteAssessAnswerByIds(ids));
    }
}
