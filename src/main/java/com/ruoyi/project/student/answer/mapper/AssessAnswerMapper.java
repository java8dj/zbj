package com.ruoyi.project.student.answer.mapper;

import java.util.List;
import com.ruoyi.project.student.answer.domain.AssessAnswer;

/**
 * 答案Mapper接口
 * 
 * @author aa
 * @date 2021-01-29
 */
public interface AssessAnswerMapper 
{
    /**
     * 查询答案
     * 
     * @param id 答案ID
     * @return 答案
     */
    public AssessAnswer selectAssessAnswerById(Long id);

    /**
     * 查询答案列表
     * 
     * @param assessAnswer 答案
     * @return 答案集合
     */
    public List<AssessAnswer> selectAssessAnswerList(AssessAnswer assessAnswer);

    /**
     * 新增答案
     * 
     * @param assessAnswer 答案
     * @return 结果
     */
    public int insertAssessAnswer(AssessAnswer assessAnswer);

    /**
     * 修改答案
     * 
     * @param assessAnswer 答案
     * @return 结果
     */
    public int updateAssessAnswer(AssessAnswer assessAnswer);

    /**
     * 删除答案
     * 
     * @param id 答案ID
     * @return 结果
     */
    public int deleteAssessAnswerById(Long id);

    /**
     * 批量删除答案
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAssessAnswerByIds(String[] ids);
}
