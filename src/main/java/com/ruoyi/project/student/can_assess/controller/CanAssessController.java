package com.ruoyi.project.student.can_assess.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.project.student.coverAssessQuestionRef.domain.CoverAssessQuestionRef;
import com.ruoyi.project.student.coverAssessQuestionRef.service.ICoverAssessQuestionRefService;
import com.ruoyi.project.student.question.domain.Question;
import com.ruoyi.project.student.question.service.IQuestionService;
import com.ruoyi.project.system.post.domain.Post;
import com.ruoyi.project.system.post.service.IPostService;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.can_assess.domain.CanAssess;
import com.ruoyi.project.student.can_assess.service.ICanAssessService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import org.springframework.web.servlet.ModelAndView;

/**
 * 评测列表Controller
 * 
 * @author aa
 * @date 2021-01-30
 */
@Controller
@RequestMapping("/student/can_assess")
public class CanAssessController extends BaseController
{
    private String prefix = "student/can_assess";

    @Autowired
    private ICanAssessService canAssessService;
    @Autowired
    private IQuestionService questionService;
    @Autowired
    private ICoverAssessQuestionRefService coverAssessQuestionRefService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IPostService postService;

    @RequiresPermissions("student:can_assess:view")
    @GetMapping()
    public String can_assess()
    {
        return prefix + "/can_assess";
    }

    /**
     * 查询评测列表列表
     */
    @RequiresPermissions("student:can_assess:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CanAssess canAssess)
    {
        startPage();
        User user = ShiroUtils.getSysUser();
        canAssess.setPinDeptId(user.getDeptId());
        canAssess.setPinPostId(getPostId(user));

        List<CanAssess> list = canAssessService.selectCanAssessList(canAssess);
        return getDataTable(list);
    }

    private Long getPostId(User user){
        List<Post> postList = postService.selectPostsByUserId(user.getUserId());
        postList = postList.stream().filter(Post::isFlag).collect(Collectors.toList());
        return postList.get(0).getPostId();
    }

    /**
     * 导出评测列表列表
     */
    @RequiresPermissions("student:can_assess:export")
    @Log(title = "评测列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CanAssess canAssess)
    {
        List<CanAssess> list = canAssessService.selectCanAssessList(canAssess);
        ExcelUtil<CanAssess> util = new ExcelUtil<CanAssess>(CanAssess.class);
        return util.exportExcel(list, "can_assess");
    }

    /**
     * 新增评测列表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增评测列表
     */
    @GetMapping("/edit_answer/{activityItemId}")
    public String editAnswer(@PathVariable("activityItemId") Long activityItemId, ModelMap mmap)
    {
        CanAssess canAssess = new CanAssess();
        canAssess.setActivityItemId(activityItemId);
        List<CanAssess> canAssessList = canAssessService.selectCanAssessList(canAssess);
        if(null!=canAssessList && canAssessList.size()>0){
            List<Question> questionList = questionService.selectQuestionAll();

            CoverAssessQuestionRef ref = new CoverAssessQuestionRef();
            ref.setCoverAssessId(canAssessList.get(0).getCoverAssessId());
            List<CoverAssessQuestionRef> refList= coverAssessQuestionRefService.selectCoverAssessQuestionRefList(ref);
            if(null==refList){
                refList = new ArrayList<>();
            }
            List<Long> qIdList = refList.stream().map(CoverAssessQuestionRef::getQuestionId).collect(Collectors.toList());

            questionList = questionList.stream()
                    .filter(e->qIdList.contains(e.getId()))
                    .collect(Collectors.toList());
            questionList.forEach(Question::contextToItems);
            mmap.put("activityItemId", activityItemId);
            mmap.put("questionList", questionList);


            // 被评测人
            User u = new User();
            u.setDeptId(canAssessList.get(0).getBeiDeptId());
            List<User> userList = userService.selectUserList(u);
            userList = userList.stream().filter(e->{
                return getPostId(e)==canAssessList.get(0).getBeiPostId();
            }).collect(Collectors.toList());
            mmap.put("userList", userList);
        }

        return prefix + "/edit_answer";
    }

    /**
     * 新增保存评测列表
     */
    @RequiresPermissions("student:can_assess:add")
    @Log(title = "评测列表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CanAssess canAssess)
    {
        return toAjax(canAssessService.insertCanAssess(canAssess));
    }

    /**
     * 修改评测列表
     */
    @GetMapping("/edit/{activityId}")
    public String edit(@PathVariable("activityId") Long activityId, ModelMap mmap)
    {
        CanAssess canAssess = canAssessService.selectCanAssessById(activityId);
        mmap.put("canAssess", canAssess);
        return prefix + "/edit";
    }

    /**
     * 修改保存评测列表
     */
    @RequiresPermissions("student:can_assess:edit")
    @Log(title = "评测列表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CanAssess canAssess)
    {
        return toAjax(canAssessService.updateCanAssess(canAssess));
    }

    /**
     * 删除评测列表
     */
    @RequiresPermissions("student:can_assess:remove")
    @Log(title = "评测列表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(canAssessService.deleteCanAssessByIds(ids));
    }
}
