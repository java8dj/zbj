package com.ruoyi.project.student.can_assess.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 评测列表对象 lg_v_can_assess
 * 
 * @author aa
 * @date 2021-01-30
 */
@Data
public class CanAssess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    private Long activityId;

    /** ID */
    @Excel(name = "ID")
    private Long activityItemId;

    /** 活动名称 */
    @Excel(name = "活动名称")
    private String title;

    /** 部门 */
    @Excel(name = "部门")
    private Long coverAssessId;

    /** 名称 */
    @Excel(name = "名称")
    private String beiName;

    /** 人员部门 */
    @Excel(name = "人员部门")
    private Long beiDeptId;

    /** 人员类型 */
    @Excel(name = "人员类型")
    private Long beiPostId;

    /** 可评测人部门 */
    @Excel(name = "可评测人部门")
    private Long pinDeptId;
    private String pinDeptName;

    /** 可评测人类型 */
    @Excel(name = "可评测人类型")
    private Long pinPostId;
    private String pinPostName;

    /** 评测占比(100内整数) */
    @Excel(name = "评测占比(100内整数)")
    private Long weight;

    public void setActivityId(Long activityId)
    {
        this.activityId = activityId;
    }

    public Long getActivityId()
    {
        return activityId;
    }
    public void setActivityItemId(Long activityItemId)
    {
        this.activityItemId = activityItemId;
    }

    public Long getActivityItemId()
    {
        return activityItemId;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setCoverAssessId(Long coverAssessId)
    {
        this.coverAssessId = coverAssessId;
    }

    public Long getCoverAssessId()
    {
        return coverAssessId;
    }
    public void setBeiName(String beiName)
    {
        this.beiName = beiName;
    }

    public String getBeiName()
    {
        return beiName;
    }
    public void setBeiDeptId(Long beiDeptId)
    {
        this.beiDeptId = beiDeptId;
    }

    public Long getBeiDeptId()
    {
        return beiDeptId;
    }
    public void setBeiPostId(Long beiPostId)
    {
        this.beiPostId = beiPostId;
    }

    public Long getBeiPostId()
    {
        return beiPostId;
    }
    public void setPinDeptId(Long pinDeptId)
    {
        this.pinDeptId = pinDeptId;
    }

    public Long getPinDeptId()
    {
        return pinDeptId;
    }
    public void setPinPostId(Long pinPostId)
    {
        this.pinPostId = pinPostId;
    }

    public Long getPinPostId()
    {
        return pinPostId;
    }
    public void setWeight(Long weight)
    {
        this.weight = weight;
    }

    public Long getWeight()
    {
        return weight;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("activityId", getActivityId())
            .append("activityItemId", getActivityItemId())
            .append("title", getTitle())
            .append("coverAssessId", getCoverAssessId())
            .append("beiName", getBeiName())
            .append("beiDeptId", getBeiDeptId())
            .append("beiPostId", getBeiPostId())
            .append("pinDeptId", getPinDeptId())
            .append("pinPostId", getPinPostId())
            .append("weight", getWeight())
            .toString();
    }
}
