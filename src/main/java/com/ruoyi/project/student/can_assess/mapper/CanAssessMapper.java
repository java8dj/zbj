package com.ruoyi.project.student.can_assess.mapper;

import java.util.List;
import com.ruoyi.project.student.can_assess.domain.CanAssess;

/**
 * 评测列表Mapper接口
 * 
 * @author aa
 * @date 2021-01-30
 */
public interface CanAssessMapper 
{
    /**
     * 查询评测列表
     * 
     * @param activityId 评测列表ID
     * @return 评测列表
     */
    public CanAssess selectCanAssessById(Long activityId);

    /**
     * 查询评测列表列表
     * 
     * @param canAssess 评测列表
     * @return 评测列表集合
     */
    public List<CanAssess> selectCanAssessList(CanAssess canAssess);

    /**
     * 新增评测列表
     * 
     * @param canAssess 评测列表
     * @return 结果
     */
    public int insertCanAssess(CanAssess canAssess);

    /**
     * 修改评测列表
     * 
     * @param canAssess 评测列表
     * @return 结果
     */
    public int updateCanAssess(CanAssess canAssess);

    /**
     * 删除评测列表
     * 
     * @param activityId 评测列表ID
     * @return 结果
     */
    public int deleteCanAssessById(Long activityId);

    /**
     * 批量删除评测列表
     * 
     * @param activityIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteCanAssessByIds(String[] activityIds);
}
