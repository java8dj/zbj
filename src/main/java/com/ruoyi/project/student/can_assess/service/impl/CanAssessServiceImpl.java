package com.ruoyi.project.student.can_assess.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.can_assess.mapper.CanAssessMapper;
import com.ruoyi.project.student.can_assess.domain.CanAssess;
import com.ruoyi.project.student.can_assess.service.ICanAssessService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 评测列表Service业务层处理
 * 
 * @author aa
 * @date 2021-01-30
 */
@Service
public class CanAssessServiceImpl implements ICanAssessService 
{
    @Autowired
    private CanAssessMapper canAssessMapper;

    /**
     * 查询评测列表
     * 
     * @param activityId 评测列表ID
     * @return 评测列表
     */
    @Override
    public CanAssess selectCanAssessById(Long activityId)
    {
        return canAssessMapper.selectCanAssessById(activityId);
    }

    /**
     * 查询评测列表列表
     * 
     * @param canAssess 评测列表
     * @return 评测列表
     */
    @Override
    public List<CanAssess> selectCanAssessList(CanAssess canAssess)
    {
        return canAssessMapper.selectCanAssessList(canAssess);
    }

    /**
     * 新增评测列表
     * 
     * @param canAssess 评测列表
     * @return 结果
     */
    @Override
    public int insertCanAssess(CanAssess canAssess)
    {
        return canAssessMapper.insertCanAssess(canAssess);
    }

    /**
     * 修改评测列表
     * 
     * @param canAssess 评测列表
     * @return 结果
     */
    @Override
    public int updateCanAssess(CanAssess canAssess)
    {
        return canAssessMapper.updateCanAssess(canAssess);
    }

    /**
     * 删除评测列表对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCanAssessByIds(String ids)
    {
        return canAssessMapper.deleteCanAssessByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除评测列表信息
     * 
     * @param activityId 评测列表ID
     * @return 结果
     */
    @Override
    public int deleteCanAssessById(Long activityId)
    {
        return canAssessMapper.deleteCanAssessById(activityId);
    }
}
