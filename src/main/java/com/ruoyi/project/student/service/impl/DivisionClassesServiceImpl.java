package com.ruoyi.project.student.service.impl;

import java.util.List;
import java.util.Random;

import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.project.student.domain.DivisionClasses;
import com.ruoyi.project.student.mapper.DivisionClassesMapper;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.service.IDivisionClassesService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 学生班级管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-07-25
 */
@Service
public class DivisionClassesServiceImpl implements IDivisionClassesService 
{
    private static final Logger log = LoggerFactory.getLogger(DivisionClassesServiceImpl.class);
    @Autowired
    private DivisionClassesMapper divisionClassesMapper;

    /**
     * 查询学生班级管理
     * 
     * @param id 学生班级管理ID
     * @return 学生班级管理
     */
    @Override
    public DivisionClasses selectDivisionClassesById(Long id)
    {
        return divisionClassesMapper.selectDivisionClassesById(id);
    }

    /**
     * 查询学生班级管理列表
     * 
     * @param divisionClasses 学生班级管理
     * @return 学生班级管理
     */
    @Override
    public List<DivisionClasses> selectDivisionClassesList(DivisionClasses divisionClasses)
    {
        return divisionClassesMapper.selectDivisionClassesList(divisionClasses);
    }

    /**
     * 新增学生班级管理
     * 
     * @param divisionClasses 学生班级管理
     * @return 结果
     */
    @Override
    public int insertDivisionClasses(DivisionClasses divisionClasses)
    {
        divisionClasses.setCardId(divisionClasses.getCardId().toUpperCase());
        return divisionClassesMapper.insertDivisionClasses(divisionClasses);
    }

    /**
     * 修改学生班级管理
     * 
     * @param divisionClasses 学生班级管理
     * @return 结果
     */
    @Override
    public int updateDivisionClasses(DivisionClasses divisionClasses)
    {
        divisionClasses.setCardId(divisionClasses.getCardId().toUpperCase());
        return divisionClassesMapper.updateDivisionClasses(divisionClasses);
    }

    /**
     * 删除学生班级管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDivisionClassesByIds(String ids)
    {
        return divisionClassesMapper.deleteDivisionClassesByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学生班级管理信息
     * 
     * @param id 学生班级管理ID
     * @return 结果
     */
    @Override
    public int deleteDivisionClassesById(Long id)
    {
        return divisionClassesMapper.deleteDivisionClassesById(id);
    }

    @Override
    public int insertDivisionClassesList(List<DivisionClasses> userList) {
        return divisionClassesMapper.insertDivisionClassesList(userList);
    }

    @Override
    public String importDivisionClasses(List<DivisionClasses> userList, boolean updateSupport) {
        if (StringUtils.isNull(userList) || userList.size() == 0)
        {
            throw new BusinessException("导入学生数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (DivisionClasses divisionClasses : userList)
        {
            try
            {
                // 验证是否存在这个身份证号码
                DivisionClasses u = divisionClassesMapper.selectCardId(divisionClasses.getCardId());
                if (StringUtils.isNull(u))
                {
                    this.insertDivisionClasses(divisionClasses);
                    successNum++;
                    successMsg.append("<br/>" + successNum +  " 导入成功");
                }
                else if (updateSupport)
                {
                    divisionClasses.setId(u.getId());
                    this.updateDivisionClasses(divisionClasses);
                    successNum++;
                    successMsg.append("<br/>" + successNum + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " +  " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new BusinessException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
