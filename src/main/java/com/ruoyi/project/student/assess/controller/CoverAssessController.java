package com.ruoyi.project.student.assess.controller;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.ruoyi.project.student.coverAssessQuestionRef.domain.CoverAssessQuestionRef;
import com.ruoyi.project.student.coverAssessQuestionRef.service.ICoverAssessQuestionRefService;
import com.ruoyi.project.student.question.service.IQuestionService;
import com.ruoyi.project.system.dept.service.IDeptService;
import com.ruoyi.project.system.post.service.IPostService;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.assess.domain.CoverAssess;
import com.ruoyi.project.student.assess.service.ICoverAssessService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 被评测类型Controller
 * 
 * @author ruoyi
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/assess/assess")
public class CoverAssessController extends BaseController
{
    private String prefix = "assess/assess";

    @Autowired
    private ICoverAssessService coverAssessService;
    @Autowired
    private IDeptService deptService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IQuestionService questionService;
    @Autowired
    private ICoverAssessQuestionRefService coverAssessQuestionRefService;

    @RequiresPermissions("assess:assess:view")
    @GetMapping()
    public String assess(ModelMap mmap)
    {
        mmap.put("depts", deptService.selectDeptAll());
        mmap.put("posts", postService.selectPostAll());

        return prefix + "/assess";
    }

    /**
     * 查询被评测类型列表
     */
    @RequiresPermissions("assess:assess:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CoverAssess coverAssess)
    {
        startPage();
        List<CoverAssess> list = coverAssessService.selectCoverAssessList(coverAssess);
        return getDataTable(list);
    }

    /**
     * 导出被评测类型列表
     */
    @RequiresPermissions("assess:assess:export")
    @Log(title = "被评测类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CoverAssess coverAssess)
    {
        List<CoverAssess> list = coverAssessService.selectCoverAssessList(coverAssess);
        ExcelUtil<CoverAssess> util = new ExcelUtil<CoverAssess>(CoverAssess.class);
        return util.exportExcel(list, "assess");
    }

    /**
     * 新增被评测类型
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("depts", deptService.selectDeptAll());
        mmap.put("posts", postService.selectPostAll());
        mmap.put("questions", questionService.selectQuestionAll());
        return prefix + "/add";
    }

    /**
     * 新增保存被评测类型
     */
    @RequiresPermissions("assess:assess:add")
    @Log(title = "被评测类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CoverAssess coverAssess)
    {
        return toAjax(coverAssessService.insertCoverAssess(coverAssess));
    }

    /**
     * 修改被评测类型
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("depts", deptService.selectDeptAll());
        mmap.put("posts", postService.selectPostAll());
        mmap.put("questions", questionService.selectQuestionAll());

        CoverAssessQuestionRef ref= new CoverAssessQuestionRef();
        ref.setCoverAssessId(id);
        List<CoverAssessQuestionRef> refList = coverAssessQuestionRefService.selectCoverAssessQuestionRefList(ref);

        CoverAssess coverAssess = coverAssessService.selectCoverAssessById(id);

        if(null!=refList){
            List<Long> qIds = refList.stream().map(CoverAssessQuestionRef::getQuestionId).collect(Collectors.toList());
            coverAssess.setQuestionIds(qIds);
        }

        mmap.put("coverAssess", coverAssess);
        return prefix + "/edit";
    }

    /**
     * 修改保存被评测类型
     */
    @RequiresPermissions("assess:assess:edit")
    @Log(title = "被评测类型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CoverAssess coverAssess)
    {
        return toAjax(coverAssessService.updateCoverAssess(coverAssess));
    }

    /**
     * 删除被评测类型
     */
    @RequiresPermissions("assess:assess:remove")
    @Log(title = "被评测类型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(coverAssessService.deleteCoverAssessByIds(ids));
    }
}
