package com.ruoyi.project.student.assess.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.student.coverAssessQuestionRef.domain.CoverAssessQuestionRef;
import com.ruoyi.project.student.coverAssessQuestionRef.mapper.CoverAssessQuestionRefMapper;
import com.ruoyi.project.system.dept.domain.Dept;
import com.ruoyi.project.system.dept.mapper.DeptMapper;
import com.ruoyi.project.system.post.domain.Post;
import com.ruoyi.project.system.post.mapper.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.assess.mapper.CoverAssessMapper;
import com.ruoyi.project.student.assess.domain.CoverAssess;
import com.ruoyi.project.student.assess.service.ICoverAssessService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 被评测类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-01-29
 */
@Service
public class CoverAssessServiceImpl implements ICoverAssessService 
{
    @Autowired
    private CoverAssessMapper coverAssessMapper;
    @Autowired
    private DeptMapper deptMapper;
    @Autowired
    private PostMapper postMapper;
    @Autowired
    private CoverAssessQuestionRefMapper coverAssessQuestionRefMapper;

    /**
     * 查询被评测类型
     * 
     * @param id 被评测类型ID
     * @return 被评测类型
     */
    @Override
    public CoverAssess selectCoverAssessById(Long id)
    {
        return coverAssessMapper.selectCoverAssessById(id);
    }

    /**
     * 查询被评测类型列表
     * 
     * @param coverAssess 被评测类型
     * @return 被评测类型
     */
    @Override
    public List<CoverAssess> selectCoverAssessList(CoverAssess coverAssess)
    {
        return coverAssessMapper.selectCoverAssessList(coverAssess);
    }

    /**
     * 新增被评测类型
     * 
     * @param coverAssess 被评测类型
     * @return 结果
     */
    @Override
    public int insertCoverAssess(CoverAssess coverAssess)
    {
        Dept dept = deptMapper.selectDeptById(coverAssess.getDeptId());
        Post post = postMapper.selectPostById(coverAssess.getPostId());
        String name = dept.getDeptName()+"-"+post.getPostName();
        coverAssess.setName(name);

        coverAssess.setCreateTime(DateUtils.getNowDate());
        int i = coverAssessMapper.insertCoverAssess(coverAssess);

        coverAssess.getQuestionIds().forEach(q->{
            CoverAssessQuestionRef ref = new CoverAssessQuestionRef();
            ref.setCoverAssessId(coverAssess.getId());
            ref.setQuestionId(q);
            coverAssessQuestionRefMapper.insertCoverAssessQuestionRef(ref);
        });

        return i;
    }

    /**
     * 修改被评测类型
     * 
     * @param coverAssess 被评测类型
     * @return 结果
     */
    @Override
    public int updateCoverAssess(CoverAssess coverAssess)
    {
        Dept dept = deptMapper.selectDeptById(coverAssess.getDeptId());
        Post post = postMapper.selectPostById(coverAssess.getPostId());
        String name = dept.getDeptName()+"-"+post.getPostName();
        coverAssess.setName(name);

        coverAssess.setUpdateTime(DateUtils.getNowDate());

        // 删除问题关联
        coverAssessQuestionRefMapper.deleteCoverAssessQuestionRefByCoverAssessId(coverAssess.getId());

        coverAssess.getQuestionIds().forEach(q->{
            CoverAssessQuestionRef ref = new CoverAssessQuestionRef();
            ref.setCoverAssessId(coverAssess.getId());
            ref.setQuestionId(q);
            coverAssessQuestionRefMapper.insertCoverAssessQuestionRef(ref);
        });

        return coverAssessMapper.updateCoverAssess(coverAssess);
    }

    /**
     * 删除被评测类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCoverAssessByIds(String ids)
    {
        return coverAssessMapper.deleteCoverAssessByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除被评测类型信息
     * 
     * @param id 被评测类型ID
     * @return 结果
     */
    @Override
    public int deleteCoverAssessById(Long id)
    {
        return coverAssessMapper.deleteCoverAssessById(id);
    }

    @Override
    public List<CoverAssess> selectCoverAssessAll() {
        return coverAssessMapper.selectCoverAssessAll();
    }
}
