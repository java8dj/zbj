package com.ruoyi.project.student.assess.service;

import java.util.List;
import com.ruoyi.project.student.assess.domain.CoverAssess;

/**
 * 被评测类型Service接口
 * 
 * @author ruoyi
 * @date 2021-01-29
 */
public interface ICoverAssessService 
{
    /**
     * 查询被评测类型
     * 
     * @param id 被评测类型ID
     * @return 被评测类型
     */
    public CoverAssess selectCoverAssessById(Long id);

    /**
     * 查询被评测类型列表
     * 
     * @param coverAssess 被评测类型
     * @return 被评测类型集合
     */
    public List<CoverAssess> selectCoverAssessList(CoverAssess coverAssess);

    /**
     * 新增被评测类型
     * 
     * @param coverAssess 被评测类型
     * @return 结果
     */
    public int insertCoverAssess(CoverAssess coverAssess);

    /**
     * 修改被评测类型
     * 
     * @param coverAssess 被评测类型
     * @return 结果
     */
    public int updateCoverAssess(CoverAssess coverAssess);

    /**
     * 批量删除被评测类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCoverAssessByIds(String ids);

    /**
     * 删除被评测类型信息
     * 
     * @param id 被评测类型ID
     * @return 结果
     */
    public int deleteCoverAssessById(Long id);

    List<CoverAssess> selectCoverAssessAll();
}
