package com.ruoyi.project.student.activity_item.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.activity_item.mapper.ActivityItemMapper;
import com.ruoyi.project.student.activity_item.domain.ActivityItem;
import com.ruoyi.project.student.activity_item.service.IActivityItemService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 评测活动被评测者类型Service业务层处理
 * 
 * @author aa
 * @date 2021-01-29
 */
@Service
public class ActivityItemServiceImpl implements IActivityItemService 
{
    @Autowired
    private ActivityItemMapper activityItemMapper;

    /**
     * 查询评测活动被评测者类型
     * 
     * @param id 评测活动被评测者类型ID
     * @return 评测活动被评测者类型
     */
    @Override
    public ActivityItem selectActivityItemById(Long id)
    {
        return activityItemMapper.selectActivityItemById(id);
    }

    /**
     * 查询评测活动被评测者类型列表
     * 
     * @param activityItem 评测活动被评测者类型
     * @return 评测活动被评测者类型
     */
    @Override
    public List<ActivityItem> selectActivityItemList(ActivityItem activityItem)
    {
        return activityItemMapper.selectActivityItemList(activityItem);
    }

    /**
     * 新增评测活动被评测者类型
     * 
     * @param activityItem 评测活动被评测者类型
     * @return 结果
     */
    @Override
    public int insertActivityItem(ActivityItem activityItem)
    {
        activityItem.setCreateTime(DateUtils.getNowDate());
        return activityItemMapper.insertActivityItem(activityItem);
    }

    /**
     * 修改评测活动被评测者类型
     * 
     * @param activityItem 评测活动被评测者类型
     * @return 结果
     */
    @Override
    public int updateActivityItem(ActivityItem activityItem)
    {
        activityItem.setUpdateTime(DateUtils.getNowDate());
        return activityItemMapper.updateActivityItem(activityItem);
    }

    /**
     * 删除评测活动被评测者类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActivityItemByIds(String ids)
    {
        return activityItemMapper.deleteActivityItemByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除评测活动被评测者类型信息
     * 
     * @param id 评测活动被评测者类型ID
     * @return 结果
     */
    @Override
    public int deleteActivityItemById(Long id)
    {
        return activityItemMapper.deleteActivityItemById(id);
    }
}
