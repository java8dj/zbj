package com.ruoyi.project.student.activity_item.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.activity_item.domain.ActivityItem;
import com.ruoyi.project.student.activity_item.service.IActivityItemService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 评测活动被评测者类型Controller
 * 
 * @author aa
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/student/activity_item")
public class ActivityItemController extends BaseController
{
    private String prefix = "student/activity_item";

    @Autowired
    private IActivityItemService activityItemService;

    @RequiresPermissions("student:activity_item:view")
    @GetMapping()
    public String activity_item()
    {
        return prefix + "/activity_item";
    }

    /**
     * 查询评测活动被评测者类型列表
     */
    @RequiresPermissions("student:activity_item:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ActivityItem activityItem)
    {
        startPage();
        List<ActivityItem> list = activityItemService.selectActivityItemList(activityItem);
        return getDataTable(list);
    }

    /**
     * 导出评测活动被评测者类型列表
     */
    @RequiresPermissions("student:activity_item:export")
    @Log(title = "评测活动被评测者类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ActivityItem activityItem)
    {
        List<ActivityItem> list = activityItemService.selectActivityItemList(activityItem);
        ExcelUtil<ActivityItem> util = new ExcelUtil<ActivityItem>(ActivityItem.class);
        return util.exportExcel(list, "activity_item");
    }

    /**
     * 新增评测活动被评测者类型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存评测活动被评测者类型
     */
    @RequiresPermissions("student:activity_item:add")
    @Log(title = "评测活动被评测者类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ActivityItem activityItem)
    {
        return toAjax(activityItemService.insertActivityItem(activityItem));
    }

    /**
     * 修改评测活动被评测者类型
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ActivityItem activityItem = activityItemService.selectActivityItemById(id);
        mmap.put("activityItem", activityItem);
        return prefix + "/edit";
    }

    /**
     * 修改保存评测活动被评测者类型
     */
    @RequiresPermissions("student:activity_item:edit")
    @Log(title = "评测活动被评测者类型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ActivityItem activityItem)
    {
        return toAjax(activityItemService.updateActivityItem(activityItem));
    }

    /**
     * 删除评测活动被评测者类型
     */
    @RequiresPermissions("student:activity_item:remove")
    @Log(title = "评测活动被评测者类型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(activityItemService.deleteActivityItemByIds(ids));
    }
}
