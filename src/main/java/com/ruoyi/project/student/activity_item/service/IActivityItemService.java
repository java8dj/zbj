package com.ruoyi.project.student.activity_item.service;

import java.util.List;
import com.ruoyi.project.student.activity_item.domain.ActivityItem;

/**
 * 评测活动被评测者类型Service接口
 * 
 * @author aa
 * @date 2021-01-29
 */
public interface IActivityItemService 
{
    /**
     * 查询评测活动被评测者类型
     * 
     * @param id 评测活动被评测者类型ID
     * @return 评测活动被评测者类型
     */
    public ActivityItem selectActivityItemById(Long id);

    /**
     * 查询评测活动被评测者类型列表
     * 
     * @param activityItem 评测活动被评测者类型
     * @return 评测活动被评测者类型集合
     */
    public List<ActivityItem> selectActivityItemList(ActivityItem activityItem);

    /**
     * 新增评测活动被评测者类型
     * 
     * @param activityItem 评测活动被评测者类型
     * @return 结果
     */
    public int insertActivityItem(ActivityItem activityItem);

    /**
     * 修改评测活动被评测者类型
     * 
     * @param activityItem 评测活动被评测者类型
     * @return 结果
     */
    public int updateActivityItem(ActivityItem activityItem);

    /**
     * 批量删除评测活动被评测者类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActivityItemByIds(String ids);

    /**
     * 删除评测活动被评测者类型信息
     * 
     * @param id 评测活动被评测者类型ID
     * @return 结果
     */
    public int deleteActivityItemById(Long id);
}
