package com.ruoyi.project.student.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 学生班级管理对象 division_classes
 * 
 * @author ruoyi
 * @date 2020-07-25
 */
public class DivisionClasses extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 学生姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 学生性别 */
    @Excel(name = "性别")
    private String sex;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String cardId;

    /** 原毕业学校 */
    @Excel(name = "原毕业小学")
    private String oldSchool;

    /** 原班级 */
    @Excel(name = "原小学班级")
    private String oldClass;

    /** 新班级 */
    @Excel(name = "现所分班级")
    private String newClass;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }

    public String getCardId()
    {
        return cardId;
    }
    public void setOldSchool(String oldSchool)
    {
        this.oldSchool = oldSchool;
    }

    public String getOldSchool()
    {
        return oldSchool;
    }
    public void setOldClass(String oldClass)
    {
        this.oldClass = oldClass;
    }

    public String getOldClass()
    {
        return oldClass;
    }
    public void setNewClass(String newClass)
    {
        this.newClass = newClass;
    }

    public String getNewClass()
    {
        return newClass;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("sex", getSex())
            .append("cardId", getCardId())
            .append("oldSchool", getOldSchool())
            .append("oldClass", getOldClass())
            .append("newClass", getNewClass())
            .toString();
    }
}
