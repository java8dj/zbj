package com.ruoyi.project.student.controller;

import java.util.List;

import com.google.code.kaptcha.Constants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.student.domain.DivisionClasses;
import com.ruoyi.project.system.user.domain.User;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.service.IDivisionClassesService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * 学生班级管理Controller
 * 
 * @author ruoyi
 * @date 2020-07-25
 */
@Controller
@RequestMapping("/student/divisionClasses")
public class DivisionClassesController extends BaseController
{
    private String prefix = "student";

    @Autowired
    private IDivisionClassesService divisionClassesService;

    @RequiresPermissions("student:divisionClasses:view")
    @GetMapping()
    public String divisionClasses()
    {
        return prefix + "/divisionClasses";
    }

    /**
     * 查询学生班级管理列表
     */
    @RequiresPermissions("student:divisionClasses:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DivisionClasses divisionClasses)
    {
        startPage();
        List<DivisionClasses> list = divisionClassesService.selectDivisionClassesList(divisionClasses);
        return getDataTable(list);
    }

    /**
     * 导出学生班级管理列表
     */
    @RequiresPermissions("student:divisionClasses:export")
    @Log(title = "学生班级管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DivisionClasses divisionClasses)
    {
        List<DivisionClasses> list = divisionClassesService.selectDivisionClassesList(divisionClasses);
        ExcelUtil<DivisionClasses> util = new ExcelUtil<DivisionClasses>(DivisionClasses.class);
        return util.exportExcel(list, "divisionClasses");
    }

    /**
     * 新增学生班级管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存学生班级管理
     */
    @RequiresPermissions("student:divisionClasses:add")
    @Log(title = "学生班级管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DivisionClasses divisionClasses)
    {
        divisionClasses.setCardId(divisionClasses.getCardId().toUpperCase());
        return toAjax(divisionClassesService.insertDivisionClasses(divisionClasses));
    }

    /**
     * 修改学生班级管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DivisionClasses divisionClasses = divisionClassesService.selectDivisionClassesById(id);
        mmap.put("divisionClasses", divisionClasses);
        return prefix + "/edit";
    }

    /**
     * 修改保存学生班级管理
     */
    @RequiresPermissions("student:divisionClasses:edit")
    @Log(title = "学生班级管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DivisionClasses divisionClasses)
    {
        divisionClasses.setCardId(divisionClasses.getCardId().toUpperCase());
        return toAjax(divisionClassesService.updateDivisionClasses(divisionClasses));
    }

    /**
     * 删除学生班级管理
     */
    @RequiresPermissions("student:divisionClasses:remove")
    @Log(title = "学生班级管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(divisionClassesService.deleteDivisionClassesByIds(ids));
    }

    @RequiresPermissions("student:divisionClasses:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate()
    {
        ExcelUtil<DivisionClasses> util = new ExcelUtil<DivisionClasses>(DivisionClasses.class);
        return util.importTemplateExcel("学生数据");
    }

    @Log(title = "学生班级管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("student:divisionClasses:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<DivisionClasses> util = new ExcelUtil<DivisionClasses>(DivisionClasses.class);
        List<DivisionClasses> userList = util.importExcel(file.getInputStream());
        String message = divisionClassesService.importDivisionClasses(userList, updateSupport);
        return AjaxResult.success(message);
    }

    @GetMapping("/query")
    public String query()
    {
        return prefix + "/query";
    }

    @PostMapping("/query")
    @ResponseBody
    public AjaxResult queryPost(String validateCode, String cardId)
    {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String aa = request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY)+"";

        if(!StringUtils.equals(validateCode, aa)){
            return AjaxResult.error("验证码错误");
        }

        request.getSession().removeAttribute(Constants.KAPTCHA_SESSION_KEY);

        if(StringUtils.isEmpty(cardId)){
            return AjaxResult.error("身份证错误");
        }

        DivisionClasses divisionClasses = new DivisionClasses();
        divisionClasses.setCardId(cardId);
        List<DivisionClasses> list = divisionClassesService.selectDivisionClassesList(divisionClasses);
        if(list.isEmpty())
            return AjaxResult.success("成功");
        return AjaxResult.success(list.get(0));
    }
}
