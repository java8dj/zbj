package com.ruoyi.project.student.mapper;

import java.util.List;
import com.ruoyi.project.student.domain.DivisionClasses;

/**
 * 学生班级管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-07-25
 */
public interface DivisionClassesMapper 
{
    /**
     * 查询学生班级管理
     * 
     * @param id 学生班级管理ID
     * @return 学生班级管理
     */
    public DivisionClasses selectDivisionClassesById(Long id);

    /**
     * 查询学生班级管理列表
     * 
     * @param divisionClasses 学生班级管理
     * @return 学生班级管理集合
     */
    public List<DivisionClasses> selectDivisionClassesList(DivisionClasses divisionClasses);

    /**
     * 新增学生班级管理
     * 
     * @param divisionClasses 学生班级管理
     * @return 结果
     */
    public int insertDivisionClasses(DivisionClasses divisionClasses);

    /**
     * 修改学生班级管理
     * 
     * @param divisionClasses 学生班级管理
     * @return 结果
     */
    public int updateDivisionClasses(DivisionClasses divisionClasses);

    /**
     * 删除学生班级管理
     * 
     * @param id 学生班级管理ID
     * @return 结果
     */
    public int deleteDivisionClassesById(Long id);

    /**
     * 批量删除学生班级管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDivisionClassesByIds(String[] ids);

    int insertDivisionClassesList(List<DivisionClasses> userList);

    DivisionClasses selectCardId(String cardId);
}
