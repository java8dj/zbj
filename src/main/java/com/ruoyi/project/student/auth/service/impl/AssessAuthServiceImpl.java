package com.ruoyi.project.student.auth.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.student.assess.domain.CoverAssess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.student.auth.mapper.AssessAuthMapper;
import com.ruoyi.project.student.auth.domain.AssessAuth;
import com.ruoyi.project.student.auth.service.IAssessAuthService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 可评测权限Service业务层处理
 * 
 * @author aaa
 * @date 2021-01-29
 */
@Service
public class AssessAuthServiceImpl implements IAssessAuthService 
{
    @Autowired
    private AssessAuthMapper assessAuthMapper;

    /**
     * 查询可评测权限
     * 
     * @param id 可评测权限ID
     * @return 可评测权限
     */
    @Override
    public AssessAuth selectAssessAuthById(Long id)
    {
        return assessAuthMapper.selectAssessAuthById(id);
    }

    /**
     * 查询可评测权限列表
     * 
     * @param assessAuth 可评测权限
     * @return 可评测权限
     */
    @Override
    public List<AssessAuth> selectAssessAuthList(AssessAuth assessAuth)
    {
        return assessAuthMapper.selectAssessAuthList(assessAuth);
    }

    /**
     * 新增可评测权限
     * 
     * @param assessAuth 可评测权限
     * @return 结果
     */
    @Override
    public int insertAssessAuth(AssessAuth assessAuth)
    {
        assessAuth.setCreateTime(DateUtils.getNowDate());
        return assessAuthMapper.insertAssessAuth(assessAuth);
    }

    /**
     * 修改可评测权限
     * 
     * @param assessAuth 可评测权限
     * @return 结果
     */
    @Override
    public int updateAssessAuth(AssessAuth assessAuth)
    {
        assessAuth.setUpdateTime(DateUtils.getNowDate());
        return assessAuthMapper.updateAssessAuth(assessAuth);
    }

    /**
     * 删除可评测权限对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAssessAuthByIds(String ids)
    {
        return assessAuthMapper.deleteAssessAuthByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除可评测权限信息
     * 
     * @param id 可评测权限ID
     * @return 结果
     */
    @Override
    public int deleteAssessAuthById(Long id)
    {
        return assessAuthMapper.deleteAssessAuthById(id);
    }
}
