package com.ruoyi.project.student.auth.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 可评测权限对象 lg_assess_auth
 * 
 * @author aaa
 * @date 2021-01-29
 */
@Data
public class AssessAuth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 被评测类型 */
    @Excel(name = "被评测类型")
    private Long coverAssessId;
    private String coverAssessName;

    /** 可评测人部门 */
    @Excel(name = "可评测人部门")
    private Long deptId;
    private String deptName;

    /** 可评测人类型 */
    @Excel(name = "可评测人类型")
    private Long postId;
    private String postName;

    /** 评测占比(100内整数) */
    @Excel(name = "评测占比(100内整数)")
    private Long weight;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCoverAssessId(Long coverAssessId)
    {
        this.coverAssessId = coverAssessId;
    }

    public Long getCoverAssessId()
    {
        return coverAssessId;
    }
    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getDeptId()
    {
        return deptId;
    }
    public void setPostId(Long postId)
    {
        this.postId = postId;
    }

    public Long getPostId()
    {
        return postId;
    }
    public void setWeight(Long weight)
    {
        this.weight = weight;
    }

    public Long getWeight()
    {
        return weight;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("coverAssessId", getCoverAssessId())
            .append("deptId", getDeptId())
            .append("postId", getPostId())
            .append("weight", getWeight())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
