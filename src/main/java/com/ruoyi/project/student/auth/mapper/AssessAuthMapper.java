package com.ruoyi.project.student.auth.mapper;

import java.util.List;
import com.ruoyi.project.student.auth.domain.AssessAuth;

/**
 * 可评测权限Mapper接口
 * 
 * @author aaa
 * @date 2021-01-29
 */
public interface AssessAuthMapper 
{
    /**
     * 查询可评测权限
     * 
     * @param id 可评测权限ID
     * @return 可评测权限
     */
    public AssessAuth selectAssessAuthById(Long id);

    /**
     * 查询可评测权限列表
     * 
     * @param assessAuth 可评测权限
     * @return 可评测权限集合
     */
    public List<AssessAuth> selectAssessAuthList(AssessAuth assessAuth);

    /**
     * 新增可评测权限
     * 
     * @param assessAuth 可评测权限
     * @return 结果
     */
    public int insertAssessAuth(AssessAuth assessAuth);

    /**
     * 修改可评测权限
     * 
     * @param assessAuth 可评测权限
     * @return 结果
     */
    public int updateAssessAuth(AssessAuth assessAuth);

    /**
     * 删除可评测权限
     * 
     * @param id 可评测权限ID
     * @return 结果
     */
    public int deleteAssessAuthById(Long id);

    /**
     * 批量删除可评测权限
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAssessAuthByIds(String[] ids);
}
