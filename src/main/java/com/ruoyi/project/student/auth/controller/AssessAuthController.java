package com.ruoyi.project.student.auth.controller;

import java.util.List;

import com.ruoyi.project.student.assess.service.ICoverAssessService;
import com.ruoyi.project.system.dept.mapper.DeptMapper;
import com.ruoyi.project.system.dept.service.IDeptService;
import com.ruoyi.project.system.post.mapper.PostMapper;
import com.ruoyi.project.system.post.service.IPostService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.student.auth.domain.AssessAuth;
import com.ruoyi.project.student.auth.service.IAssessAuthService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 可评测权限Controller
 * 
 * @author aaa
 * @date 2021-01-29
 */
@Controller
@RequestMapping("/assess/auth")
public class AssessAuthController extends BaseController
{
    private String prefix = "assess/auth";

    @Autowired
    private IAssessAuthService assessAuthService;
    @Autowired
    private IDeptService deptService;
    @Autowired
    private IPostService postService;
    @Autowired
    private ICoverAssessService coverAssessService;

    @RequiresPermissions("assess:auth:view")
    @GetMapping()
    public String auth(ModelMap mmap)
    {
        mmap.put("depts", deptService.selectDeptAll());
        mmap.put("posts", postService.selectPostAll());
        mmap.put("coverAssessList", coverAssessService.selectCoverAssessAll());
        return prefix + "/auth";
    }

    /**
     * 查询可评测权限列表
     */
    @RequiresPermissions("assess:auth:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(AssessAuth assessAuth)
    {
        startPage();
        List<AssessAuth> list = assessAuthService.selectAssessAuthList(assessAuth);
        return getDataTable(list);
    }

    /**
     * 导出可评测权限列表
     */
    @RequiresPermissions("assess:auth:export")
    @Log(title = "可评测权限", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(AssessAuth assessAuth)
    {
        List<AssessAuth> list = assessAuthService.selectAssessAuthList(assessAuth);
        ExcelUtil<AssessAuth> util = new ExcelUtil<AssessAuth>(AssessAuth.class);
        return util.exportExcel(list, "auth");
    }

    /**
     * 新增可评测权限
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("depts", deptService.selectDeptAll());
        mmap.put("posts", postService.selectPostAll());
        mmap.put("coverAssessList", coverAssessService.selectCoverAssessAll());
        return prefix + "/add";
    }

    /**
     * 新增保存可评测权限
     */
    @RequiresPermissions("assess:auth:add")
    @Log(title = "可评测权限", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(AssessAuth assessAuth)
    {
        return toAjax(assessAuthService.insertAssessAuth(assessAuth));
    }

    /**
     * 修改可评测权限
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("depts", deptService.selectDeptAll());
        mmap.put("posts", postService.selectPostAll());
        mmap.put("coverAssessList", coverAssessService.selectCoverAssessAll());
        AssessAuth assessAuth = assessAuthService.selectAssessAuthById(id);
        mmap.put("assessAuth", assessAuth);
        return prefix + "/edit";
    }

    /**
     * 修改保存可评测权限
     */
    @RequiresPermissions("assess:auth:edit")
    @Log(title = "可评测权限", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(AssessAuth assessAuth)
    {
        return toAjax(assessAuthService.updateAssessAuth(assessAuth));
    }

    /**
     * 删除可评测权限
     */
    @RequiresPermissions("assess:auth:remove")
    @Log(title = "可评测权限", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(assessAuthService.deleteAssessAuthByIds(ids));
    }
}
