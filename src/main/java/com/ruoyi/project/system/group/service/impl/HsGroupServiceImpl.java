package com.ruoyi.project.system.group.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.system.member.domain.HsGroupMember;
import com.ruoyi.project.system.member.mapper.HsGroupMemberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.group.mapper.HsGroupMapper;
import com.ruoyi.project.system.group.domain.HsGroup;
import com.ruoyi.project.system.group.service.IHsGroupService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 分组Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Service
public class HsGroupServiceImpl implements IHsGroupService 
{
    @Autowired
    private HsGroupMapper hsGroupMapper;
    @Autowired
    private HsGroupMemberMapper hsGroupMemberMapper;

    /**
     * 查询分组
     * 
     * @param id 分组ID
     * @return 分组
     */
    @Override
    public HsGroup selectHsGroupById(Long id)
    {
        return hsGroupMapper.selectHsGroupById(id);
    }

    /**
     * 查询分组列表
     * 
     * @param hsGroup 分组
     * @return 分组
     */
    @Override
    public List<HsGroup> selectHsGroupList(HsGroup hsGroup)
    {
        return hsGroupMapper.selectHsGroupList(hsGroup);
    }

    /**
     * 新增分组
     * 
     * @param hsGroup 分组
     * @return 结果
     */
    @Override
    public int insertHsGroup(HsGroup hsGroup)
    {
        hsGroup.setCreateTime(DateUtils.getNowDate());
        return hsGroupMapper.insertHsGroup(hsGroup);
    }

    /**
     * 修改分组
     * 
     * @param hsGroup 分组
     * @return 结果
     */
    @Override
    public int updateHsGroup(HsGroup hsGroup)
    {
        hsGroup.setUpdateTime(DateUtils.getNowDate());
        return hsGroupMapper.updateHsGroup(hsGroup);
    }

    /**
     * 删除分组对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHsGroupByIds(String ids)
    {
        return hsGroupMapper.deleteHsGroupByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分组信息
     * 
     * @param id 分组ID
     * @return 结果
     */
    @Override
    public int deleteHsGroupById(Long id)
    {
        return hsGroupMapper.deleteHsGroupById(id);
    }

    @Override
    public int addGroupAndMember(HsGroup group) {
        int result = hsGroupMapper.insertHsGroup(group);
        for (HsGroupMember member:group.getMemberList()) {
            member.setProjectId(group.getProjectId());
            member.setGroupId(group.getId());
            hsGroupMemberMapper.insertHsGroupMember(member);
        }
        return result;
    }
}
