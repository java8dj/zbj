package com.ruoyi.project.system.group.mapper;

import java.util.List;
import com.ruoyi.project.system.group.domain.HsGroup;

/**
 * 分组Mapper接口
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
public interface HsGroupMapper 
{
    /**
     * 查询分组
     * 
     * @param id 分组ID
     * @return 分组
     */
    public HsGroup selectHsGroupById(Long id);

    /**
     * 查询分组列表
     * 
     * @param hsGroup 分组
     * @return 分组集合
     */
    public List<HsGroup> selectHsGroupList(HsGroup hsGroup);

    /**
     * 新增分组
     * 
     * @param hsGroup 分组
     * @return 结果
     */
    public int insertHsGroup(HsGroup hsGroup);

    /**
     * 修改分组
     * 
     * @param hsGroup 分组
     * @return 结果
     */
    public int updateHsGroup(HsGroup hsGroup);

    /**
     * 删除分组
     * 
     * @param id 分组ID
     * @return 结果
     */
    public int deleteHsGroupById(Long id);

    /**
     * 批量删除分组
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHsGroupByIds(String[] ids);
}
