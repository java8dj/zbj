package com.ruoyi.project.system.group.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.group.domain.HsGroup;
import com.ruoyi.project.system.group.service.IHsGroupService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 分组Controller
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Controller
@RequestMapping("/system/group")
public class HsGroupController extends BaseController
{
    private String prefix = "system/group";

    @Autowired
    private IHsGroupService hsGroupService;

    @RequiresPermissions("system:group:view")
    @GetMapping()
    public String group()
    {
        return prefix + "/group";
    }

    /**
     * 查询分组列表
     */
    @RequiresPermissions("system:group:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HsGroup hsGroup)
    {
        startPage();
        List<HsGroup> list = hsGroupService.selectHsGroupList(hsGroup);
        return getDataTable(list);
    }

    /**
     * 导出分组列表
     */
    @RequiresPermissions("system:group:export")
    @Log(title = "分组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HsGroup hsGroup)
    {
        List<HsGroup> list = hsGroupService.selectHsGroupList(hsGroup);
        ExcelUtil<HsGroup> util = new ExcelUtil<HsGroup>(HsGroup.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 新增分组
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存分组
     */
    @RequiresPermissions("system:group:add")
    @Log(title = "分组", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HsGroup hsGroup)
    {
        return toAjax(hsGroupService.insertHsGroup(hsGroup));
    }

    /**
     * 修改分组
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        HsGroup hsGroup = hsGroupService.selectHsGroupById(id);
        mmap.put("hsGroup", hsGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存分组
     */
    @RequiresPermissions("system:group:edit")
    @Log(title = "分组", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HsGroup hsGroup)
    {
        return toAjax(hsGroupService.updateHsGroup(hsGroup));
    }

    /**
     * 删除分组
     */
    @RequiresPermissions("system:group:remove")
    @Log(title = "分组", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hsGroupService.deleteHsGroupByIds(ids));
    }



    /**
     * 新增人员信息
     */
    @GetMapping("/addGroupAndMember")
    public String addGroupAndMember()
    {
        return prefix + "/addGroupAndMember";
    }
    /**
     * 提交人员信息
     */
    @PostMapping("/addGroupAndMember")
    @ResponseBody
    public AjaxResult addGroupAndMemberPost()
    {
        return AjaxResult.success("aaa");
    }
}
