package com.ruoyi.project.system.project.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.project.domain.HsProject;
import com.ruoyi.project.system.project.service.IHsProjectService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 核算检测项目Controller
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Controller
@RequestMapping("/system/project")
public class HsProjectController extends BaseController
{
    private String prefix = "system/project";

    @Autowired
    private IHsProjectService hsProjectService;

    @RequiresPermissions("system:project:view")
    @GetMapping()
    public String project()
    {
        return prefix + "/project";
    }

    /**
     * 查询核算检测项目列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HsProject hsProject)
    {
        startPage();
        List<HsProject> list = hsProjectService.selectHsProjectList(hsProject);
        return getDataTable(list);
    }

    /**
     * 导出核算检测项目列表
     */
    @Log(title = "核算检测项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HsProject hsProject)
    {
        List<HsProject> list = hsProjectService.selectHsProjectList(hsProject);
        ExcelUtil<HsProject> util = new ExcelUtil<HsProject>(HsProject.class);
        return util.exportExcel(list, "project");
    }

    /**
     * 新增核算检测项目
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存核算检测项目
     */
    @Log(title = "核算检测项目", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HsProject hsProject)
    {
        return toAjax(hsProjectService.insertHsProject(hsProject));
    }

    /**
     * 修改核算检测项目
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        HsProject hsProject = hsProjectService.selectHsProjectById(id);
        mmap.put("hsProject", hsProject);
        return prefix + "/edit";
    }

    /**
     * 修改保存核算检测项目
     */
    @Log(title = "核算检测项目", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HsProject hsProject)
    {
        return toAjax(hsProjectService.updateHsProject(hsProject));
    }

    /**
     * 删除核算检测项目
     */
    @RequiresPermissions("system:project:remove")
    @Log(title = "核算检测项目", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hsProjectService.deleteHsProjectByIds(ids));
    }
}
