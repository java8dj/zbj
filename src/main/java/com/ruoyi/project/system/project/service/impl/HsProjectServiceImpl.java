package com.ruoyi.project.system.project.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.project.mapper.HsProjectMapper;
import com.ruoyi.project.system.project.domain.HsProject;
import com.ruoyi.project.system.project.service.IHsProjectService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 核算检测项目Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Service
public class HsProjectServiceImpl implements IHsProjectService 
{
    @Autowired
    private HsProjectMapper hsProjectMapper;

    /**
     * 查询核算检测项目
     * 
     * @param id 核算检测项目ID
     * @return 核算检测项目
     */
    @Override
    public HsProject selectHsProjectById(Long id)
    {
        return hsProjectMapper.selectHsProjectById(id);
    }

    /**
     * 查询核算检测项目列表
     * 
     * @param hsProject 核算检测项目
     * @return 核算检测项目
     */
    @Override
    public List<HsProject> selectHsProjectList(HsProject hsProject)
    {
        return hsProjectMapper.selectHsProjectList(hsProject);
    }

    /**
     * 新增核算检测项目
     * 
     * @param hsProject 核算检测项目
     * @return 结果
     */
    @Override
    public int insertHsProject(HsProject hsProject)
    {
        hsProject.setCreateTime(DateUtils.getNowDate());
        return hsProjectMapper.insertHsProject(hsProject);
    }

    /**
     * 修改核算检测项目
     * 
     * @param hsProject 核算检测项目
     * @return 结果
     */
    @Override
    public int updateHsProject(HsProject hsProject)
    {
        hsProject.setUpdateTime(DateUtils.getNowDate());
        return hsProjectMapper.updateHsProject(hsProject);
    }

    /**
     * 删除核算检测项目对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHsProjectByIds(String ids)
    {
        return hsProjectMapper.deleteHsProjectByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除核算检测项目信息
     * 
     * @param id 核算检测项目ID
     * @return 结果
     */
    @Override
    public int deleteHsProjectById(Long id)
    {
        return hsProjectMapper.deleteHsProjectById(id);
    }
}
