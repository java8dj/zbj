package com.ruoyi.project.system.project.mapper;

import java.util.List;
import com.ruoyi.project.system.project.domain.HsProject;

/**
 * 核算检测项目Mapper接口
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
public interface HsProjectMapper 
{
    /**
     * 查询核算检测项目
     * 
     * @param id 核算检测项目ID
     * @return 核算检测项目
     */
    public HsProject selectHsProjectById(Long id);

    /**
     * 查询核算检测项目列表
     * 
     * @param hsProject 核算检测项目
     * @return 核算检测项目集合
     */
    public List<HsProject> selectHsProjectList(HsProject hsProject);

    /**
     * 新增核算检测项目
     * 
     * @param hsProject 核算检测项目
     * @return 结果
     */
    public int insertHsProject(HsProject hsProject);

    /**
     * 修改核算检测项目
     * 
     * @param hsProject 核算检测项目
     * @return 结果
     */
    public int updateHsProject(HsProject hsProject);

    /**
     * 删除核算检测项目
     * 
     * @param id 核算检测项目ID
     * @return 结果
     */
    public int deleteHsProjectById(Long id);

    /**
     * 批量删除核算检测项目
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHsProjectByIds(String[] ids);
}
