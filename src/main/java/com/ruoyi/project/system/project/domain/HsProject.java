package com.ruoyi.project.system.project.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 核算检测项目对象 hs_project
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
public class HsProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 检测名称 */
    @Excel(name = "检测名称")
    private String name;

    /** 检测日期 */
    @Excel(name = "检测日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validDate;

    /** 当前检测项目 */
    @Excel(name = "当前检测项目")
    private Integer curr;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setValidDate(Date validDate)
    {
        this.validDate = validDate;
    }

    public Date getValidDate()
    {
        return validDate;
    }
    public void setCurr(Integer curr)
    {
        this.curr = curr;
    }

    public Integer getCurr()
    {
        return curr;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("validDate", getValidDate())
            .append("curr", getCurr())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
