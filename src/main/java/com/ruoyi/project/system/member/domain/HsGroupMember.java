package com.ruoyi.project.system.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 人员信息对象 hs_group_member
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
public class HsGroupMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 人员信息id */
    private Long id;

    /** 项目 */
    @Excel(name = "项目")
    private Long projectId;

    /** 组 */
    @Excel(name = "组")
    private Long groupId;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCode;

    /** 人员姓名 */
    @Excel(name = "人员姓名")
    private String name;

    /** 家庭住址 */
    @Excel(name = "家庭住址")
    private String address;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public Long getProjectId()
    {
        return projectId;
    }
    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }

    public Long getGroupId()
    {
        return groupId;
    }
    public void setIdCode(String idCode)
    {
        this.idCode = idCode;
    }

    public String getIdCode()
    {
        return idCode;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectId", getProjectId())
            .append("groupId", getGroupId())
            .append("idCode", getIdCode())
            .append("name", getName())
            .append("address", getAddress())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
