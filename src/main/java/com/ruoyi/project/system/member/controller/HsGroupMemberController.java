package com.ruoyi.project.system.member.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.member.domain.HsGroupMember;
import com.ruoyi.project.system.member.service.IHsGroupMemberService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 人员信息Controller
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Controller
@RequestMapping("/system/member")
public class HsGroupMemberController extends BaseController
{
    private String prefix = "system/member";

    @Autowired
    private IHsGroupMemberService hsGroupMemberService;

    @RequiresPermissions("system:member:view")
    @GetMapping()
    public String member()
    {
        return prefix + "/member";
    }

    /**
     * 查询人员信息列表
     */
    @RequiresPermissions("system:member:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HsGroupMember hsGroupMember)
    {
        startPage();
        List<HsGroupMember> list = hsGroupMemberService.selectHsGroupMemberList(hsGroupMember);
        return getDataTable(list);
    }

    /**
     * 导出人员信息列表
     */
    @RequiresPermissions("system:member:export")
    @Log(title = "人员信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HsGroupMember hsGroupMember)
    {
        List<HsGroupMember> list = hsGroupMemberService.selectHsGroupMemberList(hsGroupMember);
        ExcelUtil<HsGroupMember> util = new ExcelUtil<HsGroupMember>(HsGroupMember.class);
        return util.exportExcel(list, "member");
    }

    /**
     * 新增人员信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存人员信息
     */
    @RequiresPermissions("system:member:add")
    @Log(title = "人员信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HsGroupMember hsGroupMember)
    {
        return toAjax(hsGroupMemberService.insertHsGroupMember(hsGroupMember));
    }

    /**
     * 修改人员信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        HsGroupMember hsGroupMember = hsGroupMemberService.selectHsGroupMemberById(id);
        mmap.put("hsGroupMember", hsGroupMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存人员信息
     */
    @RequiresPermissions("system:member:edit")
    @Log(title = "人员信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HsGroupMember hsGroupMember)
    {
        return toAjax(hsGroupMemberService.updateHsGroupMember(hsGroupMember));
    }

    /**
     * 删除人员信息
     */
    @RequiresPermissions("system:member:remove")
    @Log(title = "人员信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hsGroupMemberService.deleteHsGroupMemberByIds(ids));
    }




}
