package com.ruoyi.project.system.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.member.mapper.HsGroupMemberMapper;
import com.ruoyi.project.system.member.domain.HsGroupMember;
import com.ruoyi.project.system.member.service.IHsGroupMemberService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 人员信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Service
public class HsGroupMemberServiceImpl implements IHsGroupMemberService 
{
    @Autowired
    private HsGroupMemberMapper hsGroupMemberMapper;

    /**
     * 查询人员信息
     * 
     * @param id 人员信息ID
     * @return 人员信息
     */
    @Override
    public HsGroupMember selectHsGroupMemberById(Long id)
    {
        return hsGroupMemberMapper.selectHsGroupMemberById(id);
    }

    /**
     * 查询人员信息列表
     * 
     * @param hsGroupMember 人员信息
     * @return 人员信息
     */
    @Override
    public List<HsGroupMember> selectHsGroupMemberList(HsGroupMember hsGroupMember)
    {
        return hsGroupMemberMapper.selectHsGroupMemberList(hsGroupMember);
    }

    /**
     * 新增人员信息
     * 
     * @param hsGroupMember 人员信息
     * @return 结果
     */
    @Override
    public int insertHsGroupMember(HsGroupMember hsGroupMember)
    {
        hsGroupMember.setCreateTime(DateUtils.getNowDate());
        return hsGroupMemberMapper.insertHsGroupMember(hsGroupMember);
    }

    /**
     * 修改人员信息
     * 
     * @param hsGroupMember 人员信息
     * @return 结果
     */
    @Override
    public int updateHsGroupMember(HsGroupMember hsGroupMember)
    {
        hsGroupMember.setUpdateTime(DateUtils.getNowDate());
        return hsGroupMemberMapper.updateHsGroupMember(hsGroupMember);
    }

    /**
     * 删除人员信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHsGroupMemberByIds(String ids)
    {
        return hsGroupMemberMapper.deleteHsGroupMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除人员信息信息
     * 
     * @param id 人员信息ID
     * @return 结果
     */
    @Override
    public int deleteHsGroupMemberById(Long id)
    {
        return hsGroupMemberMapper.deleteHsGroupMemberById(id);
    }
}
