package com.ruoyi.project.system.member.mapper;

import java.util.List;
import com.ruoyi.project.system.member.domain.HsGroupMember;

/**
 * 人员信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
public interface HsGroupMemberMapper 
{
    /**
     * 查询人员信息
     * 
     * @param id 人员信息ID
     * @return 人员信息
     */
    public HsGroupMember selectHsGroupMemberById(Long id);

    /**
     * 查询人员信息列表
     * 
     * @param hsGroupMember 人员信息
     * @return 人员信息集合
     */
    public List<HsGroupMember> selectHsGroupMemberList(HsGroupMember hsGroupMember);

    /**
     * 新增人员信息
     * 
     * @param hsGroupMember 人员信息
     * @return 结果
     */
    public int insertHsGroupMember(HsGroupMember hsGroupMember);

    /**
     * 修改人员信息
     * 
     * @param hsGroupMember 人员信息
     * @return 结果
     */
    public int updateHsGroupMember(HsGroupMember hsGroupMember);

    /**
     * 删除人员信息
     * 
     * @param id 人员信息ID
     * @return 结果
     */
    public int deleteHsGroupMemberById(Long id);

    /**
     * 批量删除人员信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHsGroupMemberByIds(String[] ids);
}
