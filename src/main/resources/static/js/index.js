!function(n) {
    var e = n.document,
    t = e.documentElement,
    i = 640,
    d = i / 100,
    o = "orientationchange" in n ? "orientationchange": "resize",
    a = function() {
        var n = t.clientWidth || 320;
        n > 640 && (n = 640),
        t.style.fontSize = n / d + "px"
    };
    e.addEventListener && (n.addEventListener(o, a, !1), e.addEventListener("DOMContentLoaded", a, !1))
} (window);

$(document).ready(function(){
    $("#btn").click(function(){
        var pattern = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
            var cardId=$("input[name='cardId']").val();
            var validateCode=$("input[name='validateCode']").val();
            if(cardId.length>0){
                if(!pattern.test(cardId)) {
                    layer.alert("请添加有效身份证号码且长度18位！！", {icon: 2});
                    return false;
                }
            }else {
                layer.alert("请添加身份证号!!", {icon: 2});
                return false;
            }
            cardId=cardId.toUpperCase();
            if(validateCode.length==0){
                layer.alert("请添加验证码!!", {icon: 2});
                return false;
            }
            $.ajax({
                type: "POST",
                url: "/student/divisionClasses/query",
                data: {validateCode:validateCode,cardId:cardId},
                dataType: "json",
                success: function (data) {
                    if(data.code!=0){
                        layer.alert(data.msg, {icon: 2});
                        $(".form_title").hide();
                    }else {
                        if(data.data!=null){
                            $(".form_title").show();
                            $("input[name='name']").val(data.data.name);
                            $("input[name='sex']").val(data.data.sex);
                            $("input[name='cardId']").val(data.data.cardId);
                            $("input[name='oldSchool']").val(data.data.oldSchool);
                            $("input[name='oldClass']").val(data.data.oldClass);
                            $("input[name='newClass']").val(data.data.newClass);
                        }else{
                            layer.alert("没有此数据", {icon: 2});
                        }
                    }
                    $("#vCode").click();
                }
            });
        });
});